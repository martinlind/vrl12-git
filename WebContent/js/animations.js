window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       || 
			window.webkitRequestAnimationFrame || 
			window.mozRequestAnimationFrame    || 
			window.oRequestAnimationFrame      || 
			window.msRequestAnimationFrame     || 
			function(callback){
				//fallback for no animationFrame
				window.setTimeout(callback, 1000 / 60);
			};
})(); 

function animate() {
	if (activeGameid) {
		games[activeGameid].drawShips();
		games[activeGameid].drawExplosions();
		games[activeGameid].drawMissiles();
	};
	requestAnimFrame(animate);
};

function animateReplay() {	
	if (activeReplay) {
		activeReplay.drawShips();
		activeReplay.drawExplosions();
		activeReplay.drawMissiles();
	};
	requestAnimFrame(animateReplay);
};

game.prototype.nextShipFrame = function() {
	this.player.nextShipFrame();
	
	for (var i in this.radar.fleet) {
		this.radar.fleet[i].changed = false;
		if (this.radar.fleet[i].opacity < 1) {
			if (animateCanvas) this.radar.fleet[i].opacity += 0.01;
			else this.radar.fleet[i].opacity = 1;
			this.radar.fleet[i].changed = true;
		}
	}
};

grid.prototype.nextShipFrame = function() {
	for (var i in this.fleet) {
		this.fleet[i].changed = false;
		var isDamageMax = this.fleet[i].visualDamage == (Math.pow(2, this.fleet[i].length) - 1);
		if (isDamageMax && this.fleet[i].visibility < 0.8) {
			if (animateCanvas) {
				if (this.fleet[i].visibility == 0) setTimeout('playShipSunkSound()',200);
				this.fleet[i].visibility += 0.01;				
			}
			else this.fleet[i].visibility = 0.8;
			this.fleet[i].changed = true;
		}
		if (this.fleet[i].opacity < 1) {
			if (animateCanvas) this.fleet[i].opacity += 0.01;
			else this.fleet[i].opacity = 1;
			this.fleet[i].changed = true;
		}
	}
};

grid.prototype.nextMissileFrame = function() {
	for (var j in this.missiles) {
		if (this.missiles[j].height > -160) {
			if (this.missiles[j].height == 1000) {
				if (animateCanvas) playBombDropSound();
			}
			if (animateCanvas) this.missiles[j].height -= 10;
			else this.missiles[j].height = 0;
			if (this.missiles[j].height == 0) {
				var missileX = this.missiles[j].x;
				var missileY = this.missiles[j].y;
				this.cells[missileY][missileX].missile = true;
				//explosion center
				var ax = this.x + 25*missileX + 25/2;
				var ay = this.y + 25*missileY + 25/2;
				var shipOnSquare = getShipIndexBySquare(this.fleet, missileX, missileY);
				if (!animateCanvas) this.missiles[j].height = -160;
				if (shipOnSquare != -1) {
					var dam =  getVisualDamage(this.fleet[shipOnSquare], this.cells);					
					this.fleet[shipOnSquare].visualDamage = dam;
					this.fleet[shipOnSquare].changed = true;
					this.missiles.splice(j, 1);
					if (animateCanvas) {
						if (!activeGameid) activeReplay.explosions.push(new explosion(ax, ay));
						else games[activeGameid].explosions.push(new explosion(ax, ay));
						playExplosionSound();
					}
				} else {
					if (animateCanvas) {
						if (!activeGameid) activeReplay.splashes.push(new splash(ax, ay));
						else games[activeGameid].splashes.push(new splash(ax, ay));
						playSplashSound();
					}
				}
			}
		}
	}
};

game.prototype.nextExplosionFrame = function() {
	for (var k in this.explosions) {
		if (this.explosions[k].size < 100) {
			if (animateCanvas) this.explosions[k].size += 1;
			else this.explosions[k].size = 100;
		} else {
			this.explosions.splice(k, 1);
		}
	}
	
	
	for (var l in this.splashes) {
		if (this.splashes[l].size < 100) {
			if (animateCanvas) this.splashes[l].size += 1;
			else this.splashes[l].size = 100;
		} else {
			this.splashes.splice(l, 1);
		}
	}
};

game.prototype.nextMissileFrame = function() {
	this.player.nextMissileFrame();
};

replay.prototype.nextShipFrame = function() {
	this.player1.nextShipFrame();
	this.player2.nextShipFrame();
};

replay.prototype.nextExplosionFrame = function() {
	for (var k in this.explosions) {		
		if (this.explosions[k].size < 100) {
			if (animateCanvas) this.explosions[k].size += 1;
			else this.explosions[k].size = 100;
		} else {
			this.explosions.splice(k, 1);
		}
	}
	
	
	for (var l in this.splashes) {
		if (this.splashes[l].size < 100) {
			if (animateCanvas) this.splashes[l].size += 1;
			else this.splashes[l].size = 100;
		} else {
			this.splashes.splice(l, 1);
		}
	}
};

replay.prototype.nextMissileFrame = function() {	
	this.player1.nextMissileFrame();
	this.player2.nextMissileFrame();
};

function explosion(x, y) {
	this.x = x;
	this.y = y;
	this.size = 0;
}

explosion.prototype.draw = function(ctx) {
	ctx.save();
	var alpha = Math.cos(this.size*0.0314);
	ctx.fillStyle = 'rgba(255,128,0,'+alpha+')';
	ctx.beginPath();
    ctx.arc(this.x, this.y, this.size/2, 0, Math.PI*2, true);
    ctx.closePath();
	ctx.fill();
	ctx.restore();
};

function splash(x, y) {
	this.x = x;
	this.y = y;
	this.size = 0;
}

splash.prototype.draw = function(ctx) {
	ctx.save();
	var alpha = Math.cos(this.size*0.0314);
	ctx.fillStyle = 'rgba(0,128,255,'+alpha+')';
	ctx.beginPath();
    ctx.arc(this.x, this.y, this.size/2, 0, Math.PI*2, true);
    ctx.closePath();
	ctx.fill();
	ctx.restore();
};