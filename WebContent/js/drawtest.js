var missiles = [];
var frameCount = 0;
var fps = 0;
var lastTime = new Date();

window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       || 
			window.webkitRequestAnimationFrame || 
			window.mozRequestAnimationFrame    || 
			window.oRequestAnimationFrame      || 
			window.msRequestAnimationFrame     || 
			function(callback){
				//fallback for no animationFrame
				window.setTimeout(callback, 1000 / 60);
			};
})();

function draw() {
	preRender();
	setInterval("addMissile()", 500);
	animate();	
}

function addMissile() {
	missiles.push(new missile(Math.random()*500, Math.random()*500));
}

function animate() {
	var nowTime = new Date();
	var diffTime = Math.ceil((nowTime.getTime() - lastTime.getTime()));
	if (diffTime >= 1000) {
		fps = frameCount;
		frameCount = 0.0;
		lastTime = nowTime;
	}
	frameCount++;
	$('#fps').html('FPS: '+fps);
	$('#count').html('Missile count: '+missiles.length);
	drawMissiles();
	requestAnimFrame(animate);	
};

function nextMissileFrame() {
	for (var j = 0; j < missiles.length; j++) {
		if (missiles[j].height > -160) {
			/*
			if (this.missiles[j].height == 1000) {
				playBombDropSound();
			}
			*/
			missiles[j].height -= 10;
			if (missiles[j].height == -160) {
				missiles.splice(j, 1);
			}
		}
	}
};

function drawMissiles() {
	var ctx = document.getElementById('canvas').getContext('2d');	
	nextMissileFrame();
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	ctx.save();
	for (var m = 0; m < missiles.length; m++) {
	    var missile = missiles[m];
	    drawMissileImg(ctx, missile.x, missile.y, missile.height);
	}
	ctx.restore();
};
/*
grid.prototype.nextMissileFrame = function() {
	for (var j in this.missiles) {
		if (this.missiles[j].height > -160) {
			if (this.missiles[j].height == 1000) {
				if (animateCanvas) playBombDropSound();
			}
			if (animateCanvas) this.missiles[j].height -= 10;
			else this.missiles[j].height = 0;
			if (this.missiles[j].height == 0) {
				var missileX = this.missiles[j].x;
				var missileY = this.missiles[j].y;
				this.cells[missileY][missileX].missile = true;
				//explosion center
				var ax = this.x + 25*missileX + 25/2;
				var ay = this.y + 25*missileY + 25/2;
				var shipOnSquare = getShipIndexBySquare(this.fleet, missileX, missileY);
				if (!animateCanvas) this.missiles[j].height = -160;
				if (shipOnSquare != -1) {
					var dam =  getVisualDamage(this.fleet[shipOnSquare], this.cells);					
					this.fleet[shipOnSquare].visualDamage = dam;
					this.fleet[shipOnSquare].changed = true;
					this.missiles.splice(j, 1);
					if (animateCanvas) {
						if (!activeGameid) activeReplay.explosions.push(new explosion(ax, ay));
						else games[activeGameid].explosions.push(new explosion(ax, ay));
						playExplosionSound();
					}
				} else {
					if (animateCanvas) {
						if (!activeGameid) activeReplay.splashes.push(new splash(ax, ay));
						else games[activeGameid].splashes.push(new splash(ax, ay));
						playSplashSound();
					}
				}
			}
		}
	}
};
*/
