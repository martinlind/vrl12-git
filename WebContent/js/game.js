function createEvents() {
	console.log("creating events");
	
	socket.on('updategame', function (userid, data) {
		var gameid = data.gameid;
		var type = data.type;
		if (activeGameid != gameid) $('#game'+gameid).addClass('hilitegame');
		if (type == "coord") {
			games[gameid].processEnemyBomb(data);
		} else if (type == "cell") {
			games[gameid].processEnemyInfo(data);
		} else if (type == "ship") {
			games[gameid].processEnemyShip(data);
		} else if (type == "ready") {
			games[gameid].processEnemyReady(data);
		} else if (type == "end") {
			games[gameid].processEnemyWin(data);		
		} else if (type == "quit") {
			games[gameid].processEnemyQuit(data);		
		}
		if (gameid == activeGameid) games[activeGameid].drawGrid();
	});
	$("body").on("mousemove", "#canvas", function(evt) {
    	if (activeGameid) {
    		var mousePos = getMousePos(evt);
    		games[activeGameid].processMouseMove(mousePos);
    		games[activeGameid].drawGrid();
		}
    });
    
	$("body").on("click", "canvas", function(evt) {
		if (activeGameid) {
			var mousePos = getMousePos(evt);
			games[activeGameid].processMouseClick(mousePos);
			games[activeGameid].drawGrid();
		}
    });
    
	$("body").on("mousedown", "#canvas", function(evt) {
		if (activeGameid) {
	     	var mousePos = getMousePos(evt);
	     	games[activeGameid].processMouseDown(mousePos);
	     	games[activeGameid].drawGrid();
		}
    });
    
	$("body").on("mouseup", "#canvas", function(evt) {
		if (activeGameid) {
			var mousePos = getMousePos(evt);
			games[activeGameid].processMouseUp(mousePos);
			games[activeGameid].drawGrid();
		}
    });
}

function quitGame() {
	if (activeGameid) {
		socket.emit('senddata', {type: "quit", gameid: activeGameid});		
		console.log("you lost");
		games[activeGameid].message = "You quit and lost";
		updateGameInfo("You lost game ["+activeGameid+"]");
		games[activeGameid].gameover = true;
		games[activeGameid].gameon = true;		
		endGame(activeGameid);
	}
}

function game() {
	this.gameon = false;
	this.gameover = false;
	this.won = false;
	this.playerSkip = false;
	this.playerReady = false;
	this.enemyReady = false;
	
	this.player = new grid(25.5, 25.5, true);			
	this.radar = new grid(325.5, 25.5, false);
	this.radar.fill = "rgb(0,160,0)";
	this.radar.stroke = "rgb(0,120,0)";
	
	this.select = false;
	this.selectValue = false;
	
	this.squareWidth = 25;
	this.squareHeight = 25;
	
	this.startButton = new button(475, 225, 100, 50, true, "Ready");
	this.randomButton = new button(325, 225, 120, 50, true, "Randomize");
	this.clearButton = new button(325, 150, 120, 50, true, "Clear");
	this.okButton = new button(250, 150, 100, 50, false, "Exit");
	
	this.message = "Waiting for Enemy";
	this.squareX = -1;
	this.squareY = -1;
	
	this.explosions = [];
	this.splashes = [];
	
	this.shots = [];
	
	this.player.createBoardLayout();
	
	this.clearShipLayer = true;
	this.refreshShipLayer = true;
}

game.prototype.drawGrid = function() {
	var ctxg = document.getElementById('gridlayer').getContext('2d');
	ctxg.clearRect(0, 0, ctxg.canvas.width, ctxg.canvas.height);
	var state = this.gameon ? 1 : 0;	
	
	switch (state) {
	case 0:	
		//ship placement		
		this.startButton.draw(ctxg);
		this.randomButton.draw(ctxg);
		this.clearButton.draw(ctxg);
	
		this.player.drawGrid(ctxg);
		drawDocks(ctxg, this.player.fleet);
		
		break;
	case 1:
		//actual gameplay		
		this.player.drawGrid(ctxg);
		this.radar.drawGrid(ctxg);
		
		if (!this.gameover) {
			if (this.playerSkip) {
				this.message = "Enemy's turn";
			} else {
				this.message = "Your turn";
			}
		}		
		break;
	}
	writeMessage(ctxg, this.message, 325, 295, 250);
};

game.prototype.drawShips = function() {	
	var ctxs = document.getElementById('shiplayer').getContext('2d');	
	if (this.clearShipLayer) {
		ctxs.clearRect(0, 0, ctxs.canvas.width, ctxs.canvas.height);
		this.clearShipLayer = false;
	}
	
	if (this.refreshShipLayer) {
		for (var s = 0; s < this.player.fleet.length; s++) {
			this.player.fleet[s].changed = true;
	    }
		for (var r = 0; r < this.radar.fleet.length; r++) {
			this.radar.fleet[r].changed = true;
	    }
		this.refreshShipLayer = false;
	}
	
	var state = this.gameon ? 1 : 0;
	
	switch (state) {
	case 0:	
		//ship placement	
		this.player.drawFleet(ctxs);		
		break;
	case 1:
		//actual gameplay
		this.player.drawFleet(ctxs);
		this.radar.drawFleet(ctxs);
		break;
	}
	this.nextShipFrame();
};

game.prototype.drawExplosions = function() {
	var ctxe = document.getElementById('explosionlayer').getContext('2d');	
	ctxe.clearRect(0, 0, ctxe.canvas.width, ctxe.canvas.height);	
	var state = this.gameon ? 1 : 0;	
	this.nextExplosionFrame();
	
	if (state == 1) {
		//actual gameplay
		for (var k in this.explosions) {
			this.explosions[k].draw(ctxe);
		}
				
		for (var l in this.splashes) {
				this.splashes[l].draw(ctxe);
		}
	}
};

game.prototype.drawMissiles = function() {
	var ctxm = document.getElementById('missilelayer').getContext('2d');	
	ctxm.clearRect(0, 0, ctxm.canvas.width, ctxm.canvas.height);	
	var state = this.gameon ? 1 : 0;
	
	this.nextMissileFrame();
	
	if (state == 1) {
		//actual gameplay	
		this.player.drawMissiles(ctxm);
		this.radar.drawMissiles(ctxm);
	}
};

game.prototype.draw = function() {	
	var ctxs = document.getElementById('shiplayer').getContext('2d');
	var ctxe = document.getElementById('explosionlayer').getContext('2d');
	var ctxm = document.getElementById('missilelayer').getContext('2d');
	
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	
	var state = this.gameon ? 1 : 0;
	
	var mouseSquareX = 25.5+this.squareX*this.squareWidth;
	var mouseSquareY = 25.5+this.squareY*this.squareHeight;
	
	/* TODO: should implement and how to implement?
	var playerSquareX = this.player.x + this.squareX*this.squareWidth;
	var playerSquareY = this.player.y + this.squareY*this.squareHeight;
	var radarSquareX = this.radar.x + this.squareX*this.squareWidth;
	var radarSquareY = this.radar.y + this.squareY*this.squareHeight;
	*/
	
	switch (state) {
	case 0:	
		//ship placement		
		this.startButton.draw(ctx);
		this.randomButton.draw(ctx);
		this.clearButton.draw(ctx);
		
		this.nextFrame();
	
		this.player.draw(ctx);
		drawDocks(ctx, this.player.fleet);
		
		if(onBoard(this.squareX, this.squareY)) {
			ctx.strokeStyle = "rgb(255,127,0)";
			ctx.strokeRect(mouseSquareX,mouseSquareY,this.squareWidth,this.squareHeight);
		}
		
		break;
	case 1:
		//actual gameplay
		
		this.nextFrame();
		
		this.player.draw(ctx);
		this.radar.draw(ctx);
		
		for (var k in this.explosions) {
			this.explosions[k].draw(ctx);
		}
				
		for (var l in this.splashes) {
				this.splashes[l].draw(ctx);
		}
		
		if (!this.gameover) {
			if (this.playerSkip) {
				this.message = "Enemy's turn";
			} else {
				this.message = "Your turn";
			}
		}
		
		if (onBoard(this.squareX-12, this.squareY) && !this.radar.cells[this.squareY][this.squareX-12].selected) {
			 ctx.strokeStyle = "rgb(0,255,0)";
		     ctx.strokeRect(mouseSquareX,mouseSquareY,this.squareWidth,this.squareHeight);
		}
		
		break;
	}
	writeMessage(ctx, this.message, 325, 295, 250);
};

game.prototype.drawSquareHighlight = function() {
	var ctx = document.getElementById('canvas').getContext('2d');	
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);	
	var state = this.gameon ? 1 : 0;	
	var mouseSquareX = 25.5+this.squareX*this.squareWidth;
	var mouseSquareY = 25.5+this.squareY*this.squareHeight;
		
	switch (state) {
	case 0:		
		if(onBoard(this.squareX, this.squareY)) {
			ctx.strokeStyle = "rgb(255,127,0)";
			ctx.strokeRect(mouseSquareX,mouseSquareY,this.squareWidth,this.squareHeight);
		}		
		break;
	case 1:		
		if (onBoard(this.squareX-12, this.squareY) && !this.radar.cells[this.squareY][this.squareX-12].selected) {
			 ctx.strokeStyle = "rgb(0,255,0)";
		     ctx.strokeRect(mouseSquareX,mouseSquareY,this.squareWidth,this.squareHeight);
		}		
		break;
	}
};

game.prototype.processEnemyBomb = function(data) {
	var x = data.x;
	var y = data.y;
	this.shots.push(new shot(false, x, y));
	this.playerSkip = false;
	this.player.cells[y][x].bomb = true;
	var newMissile = new missile(x, y);
	this.player.missiles.push(newMissile);
	var shipIndex = getShipIndexBySquare(this.player.fleet, x, y);
	var shipHit = false;
	var shipo = null;
	if (shipIndex != -1) {
		//ship hit
		shipHit = true;
		this.playerSkip = true;
		var shipDamage = getDamage(this.player.fleet[shipIndex], this.player.cells);
		this.player.fleet[shipIndex].damage = shipDamage;
		this.player.fleet[shipIndex],changed = true;
		if (!animateCanvas) this.player.fleet[shipIndex].visualDamage = shipDamage;
		if (shipDamage == Math.pow(2, this.player.fleet[shipIndex].length)-1) {
			//ship sunk
			this.player.fleet[shipIndex].sunk = true;
			shipo = this.player.fleet[shipIndex];
		} else {
			this.player.fleet[shipIndex].sunk = false;
		}
	}
	socket.emit('senddata',{type: "cell", gameid: data.gameid, x: x, y: y, hit: shipHit, sunkShip: shipo});
};

game.prototype.processEnemyInfo = function(data) {
	this.playerSkip = true;
	if (data.hit) {
		this.playerSkip = false;
		this.radar.cells[data.y][data.x].ship ^= true;
		var sunkShip = data.sunkShip;
		if (sunkShip) {
			var newShip = new ship(sunkShip.x, sunkShip.y, sunkShip.vertical, sunkShip.length, 0, false, "#00FF00", "#00FF00");
			this.radar.fleet.push(newShip);
			this.radar.updateRadar();
			if (this.radar.fleet.length == 10) {
				console.log("you won");
				socket.emit('senddata',{type: "end", gameid: data.gameid});
				this.gameover = true;
	        	this.message = "You won";
	        	updateGameInfo("You won game ["+data.gameid+"]");
	        	this.won = true;
	        	endGame(data.gameid);
			}
		}
	}
};

game.prototype.processEnemyShip = function(data) {
	console.log("process enemy ship");
	this.playerSkip = false;
	var sunkShip = data.ship;
	var newShip = new ship(sunkShip.x, sunkShip.y, sunkShip.vertical, sunkShip.length, 0, false, "#00FF00", "#00FF00");
	this.radar.fleet.push(newShip);
	this.radar.updateRadar();
	if (this.radar.fleet.length == 10) socket.emit('leavegame', data.gameid);
};

game.prototype.processEnemyReady = function(data) {
	console.log("enemy ready");
	this.enemyReady = data.ready;
	if (!this.gameon) {
		if (this.enemyReady) this.message = "Enemy Ready";
		else this.message = "Waiting for enemy";
	}
	if (this.playerReady && this.enemyReady) {
		console.log("game started");
		this.startButton.enabled = false;
		this.randomButton.enabled = false;
		this.clearButton.enabled = false;
		this.gameon = true;
		this.player.gameon = true;
	}
};

game.prototype.processEnemyWin = function(data) {	
	console.log("you lost");
	endGame(data.gameid);
	this.message = "You lost";	
	updateGameInfo("You lost game ["+data.gameid+"]");
	this.gameover = true;
	//socket.emit('leavegame', data.gameid);
};

game.prototype.processEnemyQuit = function(data) {
	console.log("you won");
	endGame(data.gameid, false);
	this.message = "You won, enemy quit";	
	updateGameInfo("You won game ["+data.gameid+"]");
	this.gameon = true;
	this.gameover = true;
	this.won = true;
	socket.emit('quitgame', data.gameid);
};

game.prototype.processMouseMove = function(mousePos) {
	var squareX = Math.floor((mousePos.x - 26)/this.squareWidth);
	var squareY = Math.floor((mousePos.y - 26)/this.squareHeight);
	this.squareX = squareX;
	this.squareY = squareY;
	//for debugging
    //this.message = "Mouse position: " + mousePos.x + "," + mousePos.y + "," + squareX + "," + squareY;
	
	this.drawSquareHighlight();
	
	if (!this.gameover) {
    	  	
	    if (!this.gameon && this.select && onBoard(squareX, squareY)) {
	    	this.player.cells[squareY][squareX].selected = this.selectValue;
        }
	    
    	if (!this.gameon) {
    		 if (this.startButton.onButton(mousePos.x, mousePos.y)) {	
    	   		if (this.select) this.startButton.click = true;
    	   		this.startButton.hover = true;
    	    } else {
    	    	this.startButton.click = false;
    	    	this.startButton.hover = false;
	    	}
    	    if (this.randomButton.onButton(mousePos.x, mousePos.y)) {
    	    	if (this.select) this.randomButton.click = true;
    	    	this.randomButton.hover = true;
	    	} else {
	    		this.randomButton.click = false;
	    		this.randomButton.hover = false;
	    	}
    	    if (this.clearButton.onButton(mousePos.x, mousePos.y)) {
    	    	if (this.select) this.clearButton.click = true;
    	    	this.clearButton.hover = true;
	    	} else {
	    		this.clearButton.click = false;
	    		this.clearButton.hover = false;
	    	}
    	}
	} else {
		if (this.okButton.onButton(mousePos.x, mousePos.y)) {	
			if (this.select) this.okButton.click = true;
			this.okButton.hover = true;
		} else {
			this.okButton.click = false;
			this.okButton.hover = false;
    	}
	}
};

game.prototype.processMouseClick = function(mousePos) {
	var squareX = Math.floor((mousePos.x - 26)/this.squareWidth);
    var squareY = Math.floor((mousePos.y - 26)/this.squareHeight);
    //for debugging
    //this.message = "Click: " + mousePos.x + "," + mousePos.y + "," + squareX + "," + squareY;
    if(!this.playerSkip && this.gameon && onBoard(squareX-12, squareY) && !this.radar.cells[squareY][squareX-12].selected) {
      	this.playerSkip = true;
      	this.radar.cells[squareY][squareX-12].selected = true;
        console.log("senddata");
        socket.emit('senddata', {type: "coord", gameid: activeGameid, x: squareX-12, y: squareY});
        this.shots.push(new shot(true, squareX-12, squareY));
    }
    if (this.startButton.enabled && this.startButton.onButton(mousePos.x, mousePos.y)) {
    	if (this.playerReady) {
    		console.log('not ready');
    		socket.emit('senddata',{type: "ready", gameid: activeGameid, ready: false});
    		this.playerReady = false;
    		this.startButton.active = false;
    	} else {
    		console.log('ready');
    		socket.emit('senddata',{type: "ready", gameid: activeGameid, ready: true});
    		this.playerReady = true;
    		this.startButton.active = true;
    		if (this.playerReady && this.enemyReady) {
        		console.log("game started");
        		this.startButton.enabled = false;
        		this.randomButton.enabled = false;
        		this.clearButton.enabled = false;
        		this.gameon = true;
        		this.player.gameon = true;
        	}
    	}
    } else if (this.randomButton.enabled && this.randomButton.onButton(mousePos.x, mousePos.y)) {
       	console.info('randomizing');
       	console.log('not ready');
       	socket.emit('senddata',{type: "ready", gameid: activeGameid, ready: false});       	
       	this.playerReady = false;       	
       	this.player.createBoardLayout();
       	this.clearShipLayer = true;
       	this.startButton.active = false;
       	this.startButton.enabled = true;	
       	
    } else if (this.clearButton.enabled && this.clearButton.onButton(mousePos.x, mousePos.y)) {
       	console.info('clearing');
       	
       	socket.emit('senddata',{type: "ready", gameid: activeGameid, ready: false});
       	this.playerReady = false;       	
       	this.player = new grid(25.5, 25.5, true);
       	this.clearShipLayer = true;
       	this.startButton.active = false;
   		this.startButton.enabled = false;
    		
    } else if (this.okButton.enabled && this.okButton.onButton(mousePos.x, mousePos.y)) {
    	this.okButton.disabled;
    }
};

game.prototype.processMouseDown = function(mousePos) {
	var squareX = Math.floor((mousePos.x - 26)/this.squareWidth);
    var squareY = Math.floor((mousePos.y - 26)/this.squareHeight);
  	this.select = true;        	
   	if(!this.gameon && squareX>=0 && squareX<=9 && squareY>=0 && squareY<=9) {
   		this.selectValue = !this.player.cells[squareY][squareX].selected;
   		this.player.cells[squareY][squareX].selected = this.selectValue;
    }
   	if (!this.gameover && !this.gameon) {
   		if (this.startButton.onButton(mousePos.x, mousePos.y)) {	    	   
   			this.startButton.click = true;
    	} else {
    		this.startButton.click = false;
   	    }
    	if (this.randomButton.onButton(mousePos.x, mousePos.y)) {	    	   
    		this.randomButton.click = true;
   	    } else {
   	    	this.randomButton.click = false;
   	    }
    	if (this.clearButton.onButton(mousePos.x, mousePos.y)) {	    	   
    		this.clearButton.click = true;
   	    } else {
   	    	this.clearButton.click = false;
   	    }
   	} else {
   		if (this.okButton.onButton(mousePos.x, mousePos.y)) {	    	   
   			this.okButton.click = true;
	    } else {
	    	this.okButton.click = false;
	    }
	}
};

game.prototype.processMouseUp = function(mousePos) {
	var squareX = Math.floor((mousePos.x - 26)/this.squareWidth);
    var squareY = Math.floor((mousePos.y - 26)/this.squareHeight);
	if (!this.gameover && !this.gameon) {
   		this.startButton.click = false;
   		this.randomButton.click = false;
   		this.clearButton.click = false;
   		if (onBoard(squareX, squareY)) {
   			this.player.fitShips();
   			this.clearShipLayer = true;
        	if (this.player.allSet) {
        		this.startButton.enabled = true;    		
        	} else {
        		this.startButton.active = false;
        		this.startButton.enabled = false;
        		console.log('not ready');
               	socket.emit('senddata',{type: "ready", gameid: activeGameid, ready: false});       	
               	this.playerReady = false;    
        	}
       	}
   	} else {
   		this.okButton.click = false;
   	}       	
	this.select = false;
};

/**
 * Gets ship index in array by square
 * @param ships array of ships
 * @param x square x coordinate
 * @param y square y coordinate
 * @returns ship index if found, -1 otherwise
 */
function getShipIndexBySquare(ships, x, y) {
	for (var s in ships) {
		var sx = ships[s].x;
		var sy = ships[s].y;
		var vertical = ships[s].vertical;
		var length = ships[s].length;
		if (x >= sx && x <= sx+(length-1)*!vertical && y >= sy && y <= sy+(length-1)*vertical) {			
			return s;
		}
	}
	return -1;
}

/**
 * Gets ship damage value by square
 * @param ship object
 * @param cells 2-dimensional array of ships
 * @returns {Number} damage value
 */

function getDamage(ship, cells) {
	var x = ship.x;
	var y = ship.y;
	var vertical = ship.vertical;
	var length = ship.length;
	var dam = 0;
	for (var i = 0; i < length; i++) {
		if (cells[y+i*vertical][x+i*!vertical].bomb) {
			dam += Math.pow(2, i);
		}
	}
	return dam;
}

function getVisualDamage(ship, cells) {
	var x = ship.x;
	var y = ship.y;
	var vertical = ship.vertical;
	var length = ship.length;
	var dam = 0;
	for (var i = 0; i < length; i++) {
		if (cells[y+i*vertical][x+i*!vertical].missile) {
			dam += Math.pow(2, i);
		}
	}
	return dam;
}

/**
 * Draws docks area
 * @param ctx context
 * @param fleet array of ships
 */
function drawDocks(ctx, fleet) {
	var counts = [4, 3, 2, 1];
	for (var n in fleet) {
		var shipo = fleet[n];
		counts[shipo.length-1]--;
	}
	ctx.fillStyle = "rgb(0,160,255)";
	ctx.fillRect(325.5,25.5,250,100);
	for(var i=0;i<counts.length;i++) {
		for(var j=0;j<counts[i];j++) {
			var size = i+1;				
			var x = 551-(j+1)*25*size;
			var y = 26+25*i;
			drawShipImg(ctx, size, x, y, false, 0, false, "#808080", "#000000", 0, 1);
		}
	}
}

/**
 * Creates grid object
 * @param x horizontal coordinate
 * @param y vertical coordinate
 * @param player is this player grid
 * @returns
 */
function grid(x, y, player) {
	this.x = x;
	this.y = y;
	this.player = player;
	this.gameon = false;
	this.allSet = false;
	this.cells = createArray(10, 10);
	for(var i=0;i<10;i++) {
		for(var j=0;j<10;j++) {
			this.cells[i][j] =  new cell(j, i);
		}
	}
	this.fleet = [];
	this.fill = "rgb(0,160,255)";
	this.stroke = "rgb(0,127,255)";
	this.missiles = [];
}

grid.prototype.draw = function(ctx) {
	ctx.save();
	for (var i = 0; i < 10; i++) {
		for ( var j = 0; j < 10; j++) {
			var x = this.x + j * 25;
			var y = this.y + i * 25;
			if (!this.player && this.cells[i][j].ship) ctx.fillStyle = "rgb(0,255,0)";
			else if (!this.player && this.cells[i][j].selected) ctx.fillStyle = this.stroke;
			else if (!this.gameon && this.player && this.cells[i][j].selected) ctx.fillStyle = this.stroke;
      		else ctx.fillStyle = this.fill;
      		ctx.fillRect(x,y,25,25);
      		ctx.strokeStyle = this.stroke;
      		ctx.strokeRect(x,y,25,25);
      		if ((!animateCanvas || !animateReplayCanvas) && this.cells[i][j].bomb && !this.cells[i][j].ship) drawMissileImg(ctx, x, y, -160);
		}
	}
	for (var s in this.fleet) {
    	var shipo = this.fleet[s];
    	drawShipImg(ctx, shipo.length, (this.x+0.5)+shipo.x*25, (this.y+0.5)+shipo.y*25, shipo.vertical, (animateCanvas ? shipo.visualDamage : shipo.damage), shipo.sunk, shipo.fill, shipo.stroke, (animateCanvas ? shipo.visibility : (shipo.sunk ? 0.8 : 0)), (animateCanvas ? shipo.opacity : 1));
    }
	if (animateCanvas || animateReplayCanvas) {
		for (var m in this.missiles) {
	    	var missile = this.missiles[m];
	    	drawMissileImg(ctx, this.x+missile.x*25, this.y+missile.y*25, missile.height);
	    }
	}
	ctx.restore();
};

grid.prototype.drawGrid = function(ctx) {
	ctx.save();
	for (var i = 0; i < 10; i++) {
		for ( var j = 0; j < 10; j++) {
			var x = this.x + j * 25;
			var y = this.y + i * 25;
			if (!this.player && this.cells[i][j].ship) ctx.fillStyle = "rgb(0,255,0)";
			else if (!this.player && this.cells[i][j].selected) ctx.fillStyle = this.stroke;
			else if (!this.gameon && this.player && this.cells[i][j].selected) ctx.fillStyle = this.stroke;
      		else ctx.fillStyle = this.fill;
      		ctx.fillRect(x,y,25,25);
      		ctx.strokeStyle = this.stroke;
      		ctx.strokeRect(x,y,25,25);
      		if ((!animateCanvas || !animateReplayCanvas) && this.cells[i][j].bomb && !this.cells[i][j].ship) drawMissileImg(ctx, x, y, -160);
		}
	}
	ctx.restore();
};

grid.prototype.drawFleet = function(ctx) {
	ctx.save();
	for (var s = 0; s < this.fleet.length; s++) {
    	var shipo = this.fleet[s];
    	if (shipo.changed) {
    		ctx.clearRect((this.x+0.5)+shipo.x*25, (this.y+0.5)+shipo.y*25, shipo.vertical ? 25 : shipo.length*25, shipo.vertical ? shipo.length*25 : 25);
    		drawShipImg(ctx, shipo.length, (this.x+0.5)+shipo.x*25, (this.y+0.5)+shipo.y*25, shipo.vertical, (animateCanvas ? shipo.visualDamage : shipo.damage), shipo.sunk, shipo.fill, shipo.stroke, (animateCanvas ? shipo.visibility : (shipo.sunk ? 0.8 : 0)), (animateCanvas ? shipo.opacity : 1));
    	}
    }
	ctx.restore();
};

grid.prototype.drawMissiles = function(ctx) {
	ctx.save();
	if (animateCanvas || animateReplayCanvas) {
		for (var m = 0; m < this.missiles.length; m++) {
	    	var missile = this.missiles[m];
	    	drawMissileImg(ctx, this.x+missile.x*25, this.y+missile.y*25, missile.height);
	    }
	}
	ctx.restore();
};

/**
 * Expands selection from ship area by one square to each side
 * Mainly to be used when ship sinks to mark squares on the sides of the ship
 */
grid.prototype.updateRadar = function() {
	for (var i = 0; i < this.fleet.length; i++) {
		var shipo = this.fleet[i];
		var verticalLength = shipo.vertical ? shipo.length : 1;
		var horizontalLength = shipo.vertical ? 1 : shipo.length;
		for (var k = shipo.y-1; k < shipo.y+verticalLength+1; k++) {
			for (var l = shipo.x-1; l < shipo.x+horizontalLength+1; l++) {
				if (onBoard(l, k)) {
					this.cells[k][l].selected = 1;
					this.cells[k][l].ship = 0;
				}
			}
		}
	}
};

/**
 * Creates random ship placement
 */
grid.prototype.createBoardLayout = function() {	
	this.fleet = [];
	var checked = createArray(10, 10);
	for (var i = 0; i < 10; i++) {
		for (var j = 0; j < 10; j++) {
			checked[i][j] = 0;
			this.cells[i][j] =  new cell(j, i);
		}
	}
	var sizes = [1, 1, 1, 1, 2, 2, 2, 3, 3, 4];
	var shipCount = 10;
	for (var i = 0; i < shipCount; i++) {
		var index = Math.floor(Math.random()*sizes.length);
		var size = sizes.splice(index, 1)[0];
		var vertical = Math.floor(Math.random()*2);
		var shipPlace = createShipPlace(size, vertical);
		var coords = indexOfArray(checked, shipPlace, true);
		if (coords.length == 0) {
			//try again
			console.info('random placement failed');
			this.createBoardLayout();
			break;
		}
		var coord = coords.splice(Math.floor(Math.random()*coords.length), 1)[0];
		//mark as checked around and under ship
		for (var k = coord[1]-1; k < coord[1]+shipPlace.length+1; k++) {
			for (var l = coord[0]-1; l < coord[0]+shipPlace[0].length+1; l++) {
				if (onBoard(l, k)) checked[k][l] = 1;
			}
		}
		//mark cells under ship
		for (var p = 0; p < size; p++) {
			var x = vertical ? coord[0] : coord[0]+p;
			var y = vertical ? coord[1]+p : coord[1];
			this.cells[y][x].selected = true;
			this.cells[y][x].ship = true;
		}
		this.fleet.push(new ship(coord[0], coord[1], vertical, size, 0, false, "#808080", "#000000"));
	}
};

/**
 * Matches ships to the user selection array
 */
grid.prototype.fitShips = function() {
	var shipCounts = [0, 0, 0, 0];
	this.fleet = [];
	var rowCount = 10;
	var colCount = 10;
	for(var i=0; i<rowCount; i++) {
		for(var j=0; j<colCount; j++) {
			this.fitShip(i, j, shipCounts);
		}
	}
	this.allSet = (shipCounts[0] == 4 && shipCounts[1] == 3 && shipCounts[2] == 2 && shipCounts[3] == 1);
};

/**
 * Fits a single ship onto specified location
 * @param i cell y coordinate
 * @param j cell x coordinate
 * @param shipCounts ship counter
 */
grid.prototype.fitShip  = function(i, j, shipCounts) {
	var s1v = createShipArray(1, true);
	var s1h = createShipArray(1, true);
	var s2v = createShipArray(2, true);
	var s2h = createShipArray(2, false);
	var s3v = createShipArray(3, true);
	var s3h = createShipArray(3, false);
	var s4v = createShipArray(4, true);
	var s4h = createShipArray(4, false);
	var shipModels = [[s1h, s1v], [s2h, s2v], [s3h, s3v], [s4h, s4v]];	
	
	this.cells[i][j].ship = false;
	
	if (this.cells[i][j].selected) {
		for(var sl=0;sl<shipModels.length;sl++) {
			for(var sv=0;sv<shipModels[0].length;sv++) {
				this.fitShipType(i, j, sl, sv, shipCounts, shipModels);
			}
		}
	} else {
		this.cells[i][j].ship = false;
	}
};

/**
 * Fits a single ship type onto specified location
 * @param i cell y coordinate
 * @param j cell x coordinate
 * @param length ship length
 * @param vertical ship orientation
 * @param shipCounts ship counter
 * @param shipModels array of ship models
 */
grid.prototype.fitShipType  = function(i, j, length, vertical, shipCounts, shipModels) {
	var shipSize = length+1;
	var limitDim = vertical ? i : j;
	shipModel = shipModels[length][vertical];
	if (!this.cells[i][j].ship && (shipCounts[length] < 5-shipSize) && (limitDim < 11-shipSize) && canShipBeOnCells(shipModel, this.cells, i, j)) {
		shipCounts[length]++;
		this.markShipCells(shipSize, vertical, i, j);
		this.fleet.push(new ship(j, i, vertical, shipSize, 0, false, "#808080", "#000000"));
	}
};

/**
 * Marks cells under the ship
 * @param size ship length
 * @param vertical ship orientation
 * @param i cell y coordinate
 * @param j cell x coordinate
 */
grid.prototype.markShipCells = function(size, vertical, i, j) {
	var limitDim = vertical ? i : j;
	for (var n=0; n<size; n++) {							
		if (limitDim < 10-n && size>n) {
			this.cells[i+n*vertical][j+n*!vertical].ship = true;
		}
	}
};

/**
 * Compares shipModel with selected cells
 * @param shipModel shipModel array
 * @param cells grid cells
 * @param i vertical index
 * @param j horizontal index
 * @returns {Boolean}
 */
function canShipBeOnCells(shipModel, cells, i, j) {
	for(var k=0;k<shipModel.length;k++) {
		for(var l=0;l<shipModel[0].length;l++) {
			var x = j+l-1;
			var y = i+k-1;
			if (onBoard(x, y) && (shipModel[k][l] != cells[y][x].selected)) return false;
		}
	}
	return true;
}

/**
 * Cell object
 * @param x horizontal coordinate
 * @param y vertical coordinate
 * @returns
 */
function cell(x, y) {
	this.x = x;
	this.y = y;
	this.selected = false;
	this.bomb = false;
	this.ship = false;
	this.missile = false;
}

/**
 * Finds if array contains smaller array
 * @param array array bigger array
 * @param subset smaller array
 * @param all whether to return all occurances or the first
 * @returns all: array of coordinate pairs,
 * !all: x and y cooordinate, (-1, -1) if no occurances found
 */
function indexOfArray(array, subset, all) {
	var isSub = true;
	indexes = [];
	for (var i = 0; i < array.length-subset.length+1; i++) {
		for (var j = 0; j < array[0].length-subset[0].length+1; j++) {
			if (array[i][j] == subset[0][0]) {
				isSub = isSubset(array, subset, i, j);
				if (all && isSub) indexes.push([j, i]);
				else if (isSub) return {'x' : j, 'y' : i};
			}
		}
	}
	if (all) return indexes;
	else return {'x' : -1, 'y' : -1};
}

/**
 * Compares array subset with smaller array
 * @param array bigger array
 * @param subset smaller array
 * @param i horizontal index
 * @param j vertical index
 * @returns {Boolean}
 */
function isSubset(array, subset, i, j) {
	for (var k = 0; k < subset.length; k++) {
		for (var l = 0; l < subset[0].length; l++) {
			if (array[k+i][l+j] != subset[k][l]) {
				return false;
			}
		}
	}
	return true;
}

/**
 * Button object
 * @param x button left-top coordinate
 * @param y button left-top coordinate
 * @param enabled is button clickable
 * @param text button text
 * @param hover is cursor on button
 * @param click is button clicked
 * @returns
 */
function button(x, y, width, height, enabled, text) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.enabled = enabled;
	this.text = text;
	this.hover = false;
	this.click = false;
	this.active = false;
}

/**
 * Draws the button
 * @param ctx canvas context
 */
button.prototype.draw = function(ctx) {
	ctx.save();
	if (this.enabled) {
		if (this.active) {
			if (this.click) ctx.fillStyle = "rgb(0,60,0)";
			else if (this.hover) ctx.fillStyle = "rgb(0,100,0)";
			else ctx.fillStyle = "rgb(0,160,0)";
		} else {
			if (this.click) ctx.fillStyle = "rgb(0,60,120)";
			else if (this.hover) ctx.fillStyle = "rgb(0,90,180)";
			else ctx.fillStyle = "rgb(0,128,255)";
		}
	}
	else ctx.fillStyle = "rgb(128,128,128)";
	ctx.fillRect(this.x, this.y, this.width, this.height);
	ctx.font = '16pt Calibri bold';
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";
	ctx.fillStyle = 'white';    
    ctx.fillText(this.text, this.x+this.width/2, this.y+this.height/2);
    ctx.restore();
};

/**
 * Check if mouse pointer is on button
 * @param mouseX mouse x coordinate
 * @param mouseY mouse y coordinate
 * @returns {Boolean}
 */
button.prototype.onButton = function(mouseX, mouseY) {
    if (mouseX >= this.x && mouseX <= this.x+this.width && mouseY >= this.y && mouseY <= this.y+this.height) {
    	return true;
    } else return false;
};

/**
 * Creates array that defines ship model,
 * zeros in the edges, ones in the middle
 * @param length ship size
 * @param vertical orientation, true = vertical, false = horizontal
 * @returns ship model array
 */
function createShipArray(length, vertical) {
	var arrayWidth = 3;
	var shipArray = vertical ? createArray(length+2, arrayWidth) : createArray(arrayWidth, length+2);
	for(var i=0; i<shipArray.length; i++) {
		for(var j=0; j<shipArray[0].length; j++) {
				var k = vertical ? i : j;
				var l = vertical ? j : i;
				shipArray[i][j] = (l==1 && k != 0 && k != length+1) ? 1 : 0;
		}
	}
	return shipArray;
}

/**
 * Creates ship sized array of zeros
 * @param length size of the ship
 * @param vertical orientation of the ship
 * @returns ship sized array
 */
function createShipPlace(length, vertical) {
	var shipArray = vertical ? createArray(length, 1) : createArray(1, length);
	for(var i=0; i<shipArray.length; i++) {
		for(var j=0; j<shipArray[0].length; j++) {
				shipArray[i][j] = 0;
		}
	}
	return shipArray;
}

/**
 * Checks if point is on board 
 * @param x column index
 * @param y row index
 * @returns {Boolean}
 */
function onBoard(x, y) {
	return (x>=0 && x<=9 && y>=0 && y<=9);
}

/**
 * Writes message on canvas
 * @param ctx canvas context
 * @param message text to be written
 * @param x bottom-left coordinate
 * @param y bottom-left coordinate
 * @param width text width, used to clear the text area before writing
 */
function writeMessage(ctx, message, x, y, width) {
	ctx.save();
	ctx.font = '12pt Calibri';
	ctx.fillStyle = 'white';
	ctx.clearRect(x-10, y-16, width, 20);
	ctx.fillText(message, x, y);
	ctx.restore();
}

/**
 * Gets mouse position relative to top-left corner of the canvas
 * @param canvas
 * @param evt
 * @returns mouse pointer coordinates
 */
function getMousePos(evt) {
    // get canvas position
	var canvas = document.getElementById('canvas');
    var obj = canvas;
    var top = 0;
    var left = 0;
    while (obj && obj.tagName != 'BODY') {
        top += obj.offsetTop;
        left += obj.offsetLeft;
        obj = obj.offsetParent;
    }
 
    // return relative mouse position
    var mouseX = evt.clientX - left + window.pageXOffset;
    var mouseY = evt.clientY - top + window.pageYOffset;
    
    return {
        x: mouseX,
        y: mouseY
    };
}

/**
 * Creates multidimensional array
 * @param length dimension lengths
 * @returns {Array}
 */
function createArray(length) {
    var a = new Array(length || 0);

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0; i < length; i++) {
            a[i] = createArray.apply(this, args);
        }
    }

    return a;
}

/**
 * This code is responsible of both retrieving and playing respective audio
 * files respective to particular events.
 */
/*
//prepare audio
var splashElems = [], splashIndex = 0;
for (var i = 0; i < 10; i++) splashElems.push(new Audio('audio/splash.ogg'));
var expElems = [], expIndex = 0;
for (var i = 0; i < 10; i++) expElems.push(new Audio('audio/explosion.ogg'));
var sunkElems = [], sunkIndex = 0;
for (var i = 0; i < 10; i++) sunkElems.push(new Audio('audio/shipsunk.ogg'));
var bombElems = [], bombIndex = 0;
for (var i = 0; i < 10; i++) bombElems.push(new Audio('audio/bombfall.ogg'));
*/

function playSplashSound(){
	playSoundFile('splash.ogg');
	/*
	if (window.chrome) splashElems[splashIndex].load();
	splashElems[splashIndex].play();
	splashIndex = (splashIndex + 1) % splashElems.length;
	*/
}

function playExplosionSound() {
	playSoundFile('explosion.ogg');
	/*
	if (window.chrome) expElems[expIndex].load();
	expElems[expIndex].play();
	expIndex = (expIndex + 1) % expElems.length;
	*/
}

function playShipSunkSound() {
	playSoundFile('shipsunk.ogg');
	/*
	if (window.chrome) sunkElems[sunkIndex].load();
	sunkElems[sunkIndex].play();
	sunkIndex = (sunkIndex + 1) % sunkElems.length;
	*/
}

function playBombDropSound() {
	playSoundFile('bombfall.ogg');
	/*
	console.log(bombIndex);
	if (window.chrome) bombElems[bombIndex].load();
	bombElems[bombIndex].play();
	bombIndex = (bombIndex + 1) % bombElems.length;
	*/
}

function playSoundFile(filename){
	if (playSounds) new Audio('audio/' + filename).play();
}