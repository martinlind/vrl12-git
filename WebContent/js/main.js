var serverURL = 'http://localhost:8082';
var bsUser = null;
var games = {};
var canvasEventsInitalized = false;
var activeGameid = null;
var joinableGameid = null;
var totalUserGamesCount = 0;
var playableGamesCount = 0;
var joinableGamesCount = 0;
var activeGamesCount = 0;
var animateCanvas = true;
var animateReplayCanvas = true;
var playSounds = false;
var replaysEnabled = false;
var replays = {};
var activeReplay = null;
var replayTimeout;
var jumpInterval;
var autoconnectInterval;
var usernameCookieLifespan = 3; //days
var TABLE_ID_LEADERBOARD = 'leaderboard';
var socket;


function initSocketio() {
	socket = io.connect(serverURL, {'reconnect': true, 'rememberTransport': false});
	if (!socket.socket.connected) autoconnectInterval = setInterval("connect()", 5000);
	
	socket.on('connect', function() {
		console.log('connect');
		clearInterval(autoconnectInterval);
		resetHeaderRight();
		initMenu();
		getLobbyData();
	});
	
	socket.on('connect_failed', function () {
		console.log('connect failed');
	});
	
	socket.on('disconnect', function () {
		console.log('disconnect');
		bsUser = null;
		offlineState();
	});
	
	socket.on('reconnecting', function () {
		console.log('reconnecting');
		$.get('/ping', function(data) {
	    });
	});
	
	socket.on('reconnect', function () {
		console.log('reconnect');
	});
	
	socket.on('reconnect_failed', function () {
		console.log('reconnect failed');
	});
	
	socket.on('connecting', function () {
		console.log('connecting');
	});
	
	socket.on('userauth', function (authdata) {
		var userid = authdata.google.user.id;
		var username = authdata.google.user.name;
		logInUser(userid, username);
		console.log(authdata.google.user.name+' logged in');
	});
	
	socket.on('logout', function (authdata) {
		console.log('logout');
		window.location = '/logout';
	});
	
	socket.on('updateusers', function(data) {
		console.log('updateusers');
		$('#users').empty();
		$.each(data, function(userid, userdata) {
			$('#users').append('<div>' + userdata.username + '</div>');
		});
		if ($('#users div').length == 0) $('#usersbox').hide();
		else $('#usersbox').fadeIn('slow');
	});
	
	socket.on('updateuserrank', function(rank) {
		console.log('updateuserrank');
		if (bsUser) {
			bsUser.rank = parseInt(rank);
			$('#playerinfo').html('Rank: '+ bsUser.rank);
		}
	});
	
	socket.on('gameinfo', function(type, game) {
		var gameid = game.id;
		console.log(type+' game '+gameid);
		var gamesToPlay = 'usergames';
		var gamesOfUserId = 'usergamesother';
		var gamesToJoinId = 'joingames';
		
		if (type == 'remove' || type == 'change') {
			$('#game'+gameid).remove();
			activeGamesCount--;
			if (gameid == joinableGameid) {
				$('#joinbutton').html('Join');
				$('#joinbutton').hide();
			}
		}
		
		if (type != 'remove') {
			activeGamesCount++;
			if(isPlayableGame(game)){
				console.log('Playable game: ' + gameid);
				appendGame(gamesToPlay, game, 'changeActiveGame('+gameid+');');
			} else if(isUserGame(game)){
				console.log('User game: ' + gameid);
				appendGame(gamesOfUserId, game, null);
			} else if(isJoinableGame(game)){
				console.log('Joinable game: ' + gameid);
				appendGame(gamesToJoinId, game, 'selectJoinableGame('+gameid+');');
			} else {
				console.log('Game that is neither user game nor joinable: ' + gameid);
			}
		}
		
		playableGamesCount = $('#usergames div').length;
		totalUserGamesCount = $('#usergamesother div').length + playableGamesCount;
		joinableGamesCount = $('#joingames div').length;	
		
		updateGamesCount();
	});
	
	socket.on('updategames', function(games) {
		console.log('updategames');
		
		var gamesToPlay = 'usergames';
		var gamesOfUserId = 'usergamesother';
		var gamesToJoinId = 'joingames';
		var activeGamesId = 'activegames';
		
		$('#' + gamesToPlay).empty();
		$('#' + gamesOfUserId).empty();
		$('#' + gamesToJoinId).empty();
		$('#' + activeGamesId).empty();
		
		totalUserGamesCount = 0;
		playableGamesCount = 0;
		joinableGamesCount = 0;
		activeGamesCount = 0;
		
		var selectedJoinableGameFound = false;
		
		$.each(games, function(gameid, game) {
			if (!selectedJoinableGameFound && gameid == joinableGameid) {
				selectedJoinableGameFound = true;
			}
			activeGamesCount++;
			if(isPlayableGame(game)){
				console.log('Playable game: ' + gameid);
				totalUserGamesCount++;
				playableGamesCount++;
				appendGame(gamesToPlay, game, 'changeActiveGame('+gameid+');');
			} else if(isUserGame(game)){
				console.log('User game: ' + gameid);
				totalUserGamesCount++;
				appendGame(gamesOfUserId, game, null);
			} else if(isJoinableGame(game)){
				console.log('Joinable game: ' + gameid);
				joinableGamesCount++;
				appendGame(gamesToJoinId, game, 'selectJoinableGame('+gameid+');');
			} else {
				console.log('Game that is neither user game nor joinable: ' + gameid);
			}
		});
		
		updateGamesCount();
		
		if (!selectedJoinableGameFound) {
			$('#joinbutton').html('Join');
			$('#joinbutton').hide();
		}
	});
	
	socket.on('updateuser', function(user) {
		console.log('updateuser');
		//bsUser = user;
	});
	
	socket.on('gamecreated', function(gameid) {
		console.log('created game');
		games[gameid] = new game();
	});
	
	socket.on('startgame', function(gameStartInfo) {
		startGame(gameStartInfo);
	});
	
	socket.on('savegame', function(gameInfo) {
		saveGame(gameInfo);
	});
	
	socket.on('updatechart', function(chartEntries) {
		fillLeaderboard(chartEntries);
	});
	
	socket.on('updatehistory', function(historyEntries) {
		console.log('History update taking place...');
		fillHistoryEntries(historyEntries);
	});
	
	socket.on('updateplayerstatus', function(userid, status) {
		$('table#' + TABLE_ID_LEADERBOARD + ' tr#' + userid + ' td.col3').html((status == 'online' ? '<b>'+status+'</b>' : status));
		console.log('Player "' + userid + '" status updated to "' + status + '".');
	});
	
	socket.on('usercheck', function(userid, username, isUserLoggedIn){
		if (!isUserLoggedIn) {
			console.log("valid username");
			logInUser(userid, username);
		} else {
			console.log(username+" already logged in");
			$('#header_right').html(username+' already logged in');
			setTimeout("resetHeaderRight()", 2000);
		}
	});
}

function connect() {
	console.log('trying to connect');
	$.get('/ping', function(data) {
		socket.socket.connect(serverURL);
     });	
}

function user(uid, username) {
	this.id = uid;
	this.username = username;
	this.games = {};
	this.rank = 0;
}

function resetHeaderRight() {
	$('#header_right').html('<a id="googlelogin" class="button signinbutton" href="/auth/google">Log in with Google</a>').hide().fadeIn('slow');
}

function offlineState() {
	$('#header_right').html('<b>Offline</b>');
	$('#playerinfo').empty();
	$('#historybutton').hide();
	$('#lobbybutton').hide();
	$('#gamesbutton').hide();
	showInfo();
}

function initLobby() {
	console.log('init lobby');
	if (bsUser) {
		$('#newgamebutton').fadeIn('slow');
	} else {
		$('#newgamebutton').hide();
		$('#joinbutton').hide();
	}
}

function initGames() {
	console.log('init games');
}

function initCanvasEvents() {
	if (!canvasEventsInitalized) {
		canvasEventsInitalized = true;
		createEvents();
	}
}

function updateGamesCount() {
	$('#gamesbutton').html('Games' + (totalUserGamesCount > 0 ? ' ('+playableGamesCount+'/'+totalUserGamesCount+')' : ''));
	$('#lobbybutton').html('Lobby' + (joinableGamesCount > 0 ? ' ('+joinableGamesCount+')' : ''));
	$('#activegamestitle').html('Games on server: '+activeGamesCount);
	
	if (playableGamesCount == 0) $('#usergamesbox').hide();
	else $('#usergamesbox').fadeIn('slow');
	if ($('#usergamesother div').length == 0) $('#usergamesotherbox').hide();
	else $('#usergamesotherbox').fadeIn('slow');
	if (joinableGamesCount == 0) $('#joingamesbox').hide();
	else $('#joingamesbox').fadeIn('slow');
}


//TODO: probably not used anymore
function addExistingUser(username) {
	socket.emit('adduser', username);
	$('.signinbutton').html("Sign Out, " + username);
	setCookie("username", username, usernameCookieLifespan);
	socket.emit('getuserrank');
}

function changeActiveGame(gameid) {
	activeGameid = gameid;
	console.log("New active game: "+ activeGameid);	
	if (activeGameid) {
		games[activeGameid].drawGrid();
		games[activeGameid].clearShipLayer = true;
		games[activeGameid].refreshShipLayer = true;
		$('#game'+gameid).removeClass('hilitegame');
		$('#quitbutton').html('Quit Game ['+activeGameid+']');
		$('#quitbutton').fadeIn('slow');
	}
	updateGameHighlight();
}

function selectJoinableGame(gameid) {
	joinableGameid = gameid;
	console.log("New joinable game: "+ joinableGameid);
	if (joinableGameid != null) {
		$('#joinbutton').html('Join ['+joinableGameid+']');
		$('#joinbutton').fadeIn('fast');
	}
	updateGameHighlight();
}

function updateGameHighlight() {
	$('.singlegame').removeClass('gameselected');
	$('#game'+activeGameid).addClass('gameselected');
	$('#game'+joinableGameid).addClass('gameselected');
}

function logInUser(userid, username) {
	bsUser = new user(userid, username);
	$('#googlelogin').html('Log out, '+username).hide().fadeIn('slow');
	$('#googlelogin').attr("href", "/logout");
	$('#historybutton').fadeIn('slow');
	$('#newgamebutton').fadeIn('slow');
	$('#lobbybutton').fadeIn('slow');
	$('#gamesbutton').fadeIn('slow');
	socket.emit('getuserrank');
}

//TODO: probably not used anymore
function isUsernameValid(username) {
	if (username != null && username != '') {
		socket.emit('checkuser', username);
	} else 
	console.log("invalid username");
}

function isUserLoggedIn(userid) {
	socket.emit('checkuser', userid);
}

function newGame() {
	if (bsUser) {
		socket.emit('newgame');
		console.log('new game');
	}
}

function joinGame() {
	if (bsUser && (joinableGameid != null)) {
		socket.emit('joingame', joinableGameid);
		console.log('join game ' + joinableGameid);
		$('#joinbutton').html('Join');
		$('#joinbutton').hide();
		joinableGameid = null;
	}
}

function startGame(gameStartInfo) {
	var gameid = gameStartInfo.gameid;
	if (bsUser.id == gameStartInfo.startingUserId) {
		console.log('Player starts the game');
		games[gameid].playerSkip = false;
	} else {
		console.log('Enemy starts the game');
		games[gameid].playerSkip = true;
	}
}

function exitGame(gameToRemove) {
	console.log('exit game');
	delete games[gameToRemove];
	if (activeGameid == gameToRemove) activeGameid = null;
}

function updateGameInfo(message) {
	$('#gameinfo').html(message);
	$('#gameinfo').hide();
	$('#gameinfo').fadeIn('slow');
}

function endGame(gameid) {
	console.log('end game');
	var game = games[gameid];
	for (var i = 0; i < game.player.fleet.length; i++) {
		var shipo = game.player.fleet[i];
		if (!shipo.sunk) socket.emit('senddata',{type: "ship", gameid: gameid, ship: shipo});
	}
	if (activeGameid == gameid) {
		$('#quitbutton').hide();
		$('#quitbutton').html('Quit Game');
	}
}

function saveGame(gameInfo) {
	console.log('save game');
	var gameid = gameInfo.id;
	var replayId = 'replay'+gameid+gameInfo.date.replace(/\D/g, ''); 
	var replayPlayer1 = gameInfo.player1;
	var replayPlayer2 = gameInfo.player2;
	replays[replayId] = new replayInfo(replayId, replayPlayer1, replayPlayer2);
	localStorage.setObject('replays', replays);
	
	var player1Fleet = games[gameid].player.fleet;
	var player2Fleet = games[gameid].radar.fleet;
	player2Fleet.player = true;
	var shots = games[gameid].shots;	
	var replayToSave = new lightReplay(player1Fleet, player2Fleet, shots);
	localStorage.setObject(replayId, replayToSave);
	loadReplays();
	
	var timeoutToGameRemove = 5000;
	setTimeout("exitGame("+gameid+")", timeoutToGameRemove);
}

function getLobbyData() {
	socket.emit('getlobby');
}

function showInfo() {
	console.log('show history');
	activeGameid = null;
	$('#info').fadeIn('fast');
	$('#quitbutton').hide();
	$('#history').hide();
	$('#lobby').hide();
	$('#games').hide();
	$('#replay').hide();
	pause();
}

function showHistory() {
	console.log('show history');
	activeGameid = null;
	$('#history').fadeIn('fast');
	$('#quitbutton').hide();
	$('#lobby').hide();
	$('#games').hide();
	$('#replay').hide();
	$('#info').hide();
	initTables();
	pause();
}

function showLobby() {
	activeGameid = null;
	$('#lobby').fadeIn('fast');
	$('#quitbutton').hide();	
	$('#history').hide();	
	$('#games').hide();
	$('#replay').hide();
	$('#info').hide();
	initLobby();
	pause();
}

function showGames() {
	$('#games').fadeIn('fast');
	$('#history').hide();
	$('#lobby').hide();
	$('#replay').hide();
	$('#info').hide();
	initGames();
	pause();
}

function showReplay() {
	$('#replay').fadeIn('fast');
	$('#games').hide();
	$('#history').hide();
	$('#lobby').hide();
	$('#info').hide();
}

function getSelected(elementId) {
    var elt = document.getElementById(elementId);
    if (elt.selectedIndex == -1) return null;
    return elt.options[elt.selectedIndex].value;
}

function initPage() {
	
	loadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js", function() {
		console.log('jquery loaded');
		$.ajaxSetup({cache: true});
		loadSocketiojs();
		$('#header_right').html('<b>Offline</b>');
		initMenu();
		$('#joinbutton').hide();
		$('#quitbutton').hide();
	});
	
	if(typeof(Storage) !== "undefined") {
		if (localStorage.replays) replays = localStorage.getObject('replays');
		replaysEnabled = true;
	}
}

function loadScript(url, callback) {
	var script = document.createElement("script");
	script.type = "text/javascript";
	
	if (script.readyState) { // IE
		script.onreadystatechange = function () {
			if (script.readyState == "loaded" || script.readyState == "complete") {
				script.onreadystatechange = null;
				callback();
			}
		};
	} else { // Others
		script.onload = function () {
			callback();
		};
	}
	script.src = url;
	document.getElementsByTagName("head")[0].appendChild(script);
}

function loadSocketiojs() {
	$.getScript("socket.io/socket.io.js").done(function(script, textStatus) {
		console.log("socket.io.js loaded");
		initSocketio();
		getLobbyData();
		loadGamejs();
		loadShipsjs();
	}).fail(function(jqxhr, settings, exception) {
		console.log("socket.io.js failed");
		console.log(exception);
	});
}

function loadGamejs() {
	$.getScript("js/game.js").done(function(script, textStatus) {
		console.log("game.js loaded");
		initCanvasEvents();
		loadReplayjs();
	}).fail(function(jqxhr, settings, exception) {
		console.log("game.js failed");
		console.log(exception);
	});
}

function loadShipsjs() {
	$.getScript("js/ships.js").done(function(script, textStatus) {
		console.log("ships.js loaded");
		preRender();
	}).fail(function(jqxhr, settings, exception) {
		console.log("ships.js failed");
		console.log(exception);
	});
}

function loadReplayjs() {
	$.getScript("js/replay.js").done(function(script, textStatus) {
		console.log("replay.js loaded");
		if (replaysEnabled) {
			loadAnimationsjs();
			loadReplays();
		}
		$.ajaxSetup({cache: false});
	}).fail(function(jqxhr, settings, exception) {
		console.log("replay.js failed");
		console.log(exception);
	});
}

function loadAnimationsjs() {
	$.getScript("js/animations.js").done(function(script, textStatus) {
		console.log("animations.js loaded");
		animate();
		animateReplay();
	}).fail(function(jqxhr, settings, exception) {
		console.log("animations.js failed");
		console.log(exception);
	});
}

function toggleAnimations() {
	animateCanvas ^= true;
	if (animateCanvas)  {
		$('#animationbutton').html('Animations ON');
		$('#animreplaybutton').html('Animations ON');
	}
	else {
		$('#animationbutton').html('Animations OFF');
		$('#animreplaybutton').html('Animations OFF');
	}
}
function toggleSounds() {
	playSounds ^= true;
	if (playSounds)  {
		$('#soundbutton').html('Sounds ON');
		$('#soundreplaybutton').html('Sounds ON');
	}
	else {
		$('#soundbutton').html('Sounds OFF');
		$('#soundreplaybutton').html('Sounds OFF');
	}
}

function initMenu() {
	if (bsUser) {
		$('#historybutton').fadeIn('slow');
		$('#lobbybutton').fadeIn('slow');
		$('#gamesbutton').fadeIn('slow');
	} else {
		$('#lobbybutton').hide();
		$('#gamesbutton').hide();
	}
}

function playPause() {
	activeReplay.autoReplayOn ^= true;
	if (activeReplay.autoReplayOn) {
		$('#playbutton').html('||');
		playReplay();
	}
	else {
		clearTimeout(replayTimeout);
		$('#playbutton').html('&gt');
	}
}

function pause() {
	if (activeReplay) activeReplay.autoReplayOn = false;
	if (replayTimeout) clearTimeout(replayTimeout);
	$('#playbutton').html('&gt');
}

function scrollReplayStep() {
	$('#steps').scrollTop($(".singlestep").height()*activeReplay.state-$("#steps").height()/3);
}

function playReplay() {
	scrollReplayStep();
	activeReplay.jump(1, true);
	updateStepHighlight();
	//activeReplay.draw();
	var timeoutBetweenSteps = 1000;
	if (activeReplay.autoReplayOn) replayTimeout = setTimeout("playReplay()", timeoutBetweenSteps);
	else $('#playbutton').html('&gt');
}

function nextState() {
	pause();
	clearInterval(jumpInterval);
	activeReplay.jump(1, true);
	updateStepHighlight();
	//activeReplay.draw();
	scrollReplayStep();
}

function prevState() {
	pause();
	clearInterval(jumpInterval);
	activeReplay.jump(-1, true);
	updateStepHighlight();
	//activeReplay.draw();
	scrollReplayStep();
}

function startState() {
	pause();
	clearInterval(jumpInterval);
	activeReplay.jumpa(0);
	updateStepHighlight();
	//activeReplay.draw();
}

function endState() {
	pause();
	clearInterval(jumpInterval);
	activeReplay.jumpa(200);
	updateStepHighlight();
	//activeReplay.draw();
}

function toState(state) {
	pause();
	clearInterval(jumpInterval);
	activeReplay.jumpa(state);
	updateStepHighlight();
	//activeReplay.draw();
}

function updateStepHighlight() {
	$('.singlestep').removeClass('hilitestep');
	$('#step'+(activeReplay.state)).addClass('hilitestep');
}

function updateReplayHighlight(elementId) {
	$('.singlereplay').removeClass('hilitereplay');
	$('#'+elementId).addClass('hilitereplay');
}

/**
 * Initializes tables for the main page.
 */
function initTables() {
	initLeaderboard();
	initHistoryEntries();
}
/**
 * Turns to the database through the server in order to get necessary data for
 * the leaderboard.
 */
function initLeaderboard(){
	socket.emit('getchart');
}

/**
 * Turns to the database through the server in order to get necessary data for
 * the history entries table.
 */
function initHistoryEntries(){
	socket.emit('gethistoryentries');
}

/**
 * Populates the leaderboard with the items from the database.
 * 
 * @param chartEntries
 */
function fillLeaderboard(chartEntries){
	removePreviousEntries(TABLE_ID_LEADERBOARD);	
	
	$.each(chartEntries, function(index, chartEntry) {
		var playerId = chartEntry.uid;
		var playerName = chartEntry.name;
		if (bsUser && bsUser.id == playerId) {
			bsUser.rank = parseInt(chartEntry.rank);
			$('#playerinfo').html('Rank: '+ bsUser.rank).hide().fadeIn('slow');
		}
		var rowClass = getRowClass(index);
		$('#' + TABLE_ID_LEADERBOARD).append('<tr class="' + rowClass + '" id="'+ playerId +'">'
				+ '<td class="col1 cell">' +  playerName + '</td>'
				+ '<td class="col2 cell">' + chartEntry.rank + '</td>'
				+ '<td class="col3 cell">' + (chartEntry.online == 'online' ? '<b>'+chartEntry.online+'</b>' : chartEntry.online) + '</td>'
				+ '</tr>');
	});
}

/**
 * Populates the history entries table with the items from the database.
 * 
 * @param chartEntries
 */
function fillHistoryEntries(historyEntries){
	var tableId = 'historytable';
	removePreviousEntries(tableId);	
	
	$.each(historyEntries, function(index, historyEntry) {
		var rowClass = getRowClass(index);
		$('#' + tableId).append('<tr class="' + rowClass + '">'
				+ '<td class="col1 cell">' + formatDate(historyEntry.begin_time)  + '</td>'
				+ '<td class="col2 cell">' + historyEntry.winner.name + '</td>'
				+ '<td class="col3 cell">' + historyEntry.loser.name + '</td>'
				+ '</tr>');
	});
}

function getRowClass(index){
	return index % 2 == 0 ? 'rowa' : 'rowb';
}

function formatDate(dateString) {
	var date = new Date(dateString);
	return date.getDate() + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() + ' ' + date.getHours() + ':' + ('0' + (date.getMinutes() + 1)).slice(-2);
}

/**
 * Removes previous entries, currently intended for leaderboard and history
 * tables.
 * 
 * @param tableId
 */
function removePreviousEntries(tableId){
	$('table#' + tableId + ' tr.rowa').remove();
	$('table#' + tableId + ' tr.rowb').remove();
}

/**
 * Appends game to select (according to ID) box in HTML file.
 * 
 * @param elementid
 * @param game
 * @param onclick
 */
function appendGame(elementid, game, onclick) {
	$('#'+elementid).append('<div id="game'+game.id+'" class="singlegame" '
			+(onclick ? 'onclick="'+onclick+'"' : '')+'>['+game.id+'] '+game.player1.name+' '
			+(game.player2 ? 'vs '+game.player2.name : '')+'</div>');
}

/**
 * Returns true, if game is playable.
 * 
 * @param game
 * @returns {Boolean}
 */
function isPlayableGame(game) {
	if(bsUser && isUserGame(game) && !isJoinableGame(game)){
		return true;
	}
	console.log('isPlayableGame returns false!!!');
	return false;
}

/**
 * Returns true, if game belongs to the particular user.
 * 
 * @param game
 * @returns {Boolean}
 */
function isUserGame(game) {
	if(bsUser && (game.player1 && bsUser.id == game.player1.id || game.player2 && bsUser.id == game.player2.id)){
		return true;
	}
	console.log('isUserGame returns false!!!');
	return false;
}

/**
 * Returns true, if user can join particular game.
 * 
 * @param game
 * @returns {Boolean}
 */
function isJoinableGame(game) {
	if(game.player1 == null || game.player2 == null){
		return true;
	}
	return false;
}

function setCookie(cookieName, cookieValue, lifespanInDays, validDomain) {
	console.log("set cookie");
	var domainString = validDomain ? ("; domain=" + validDomain) : '' ;
	document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) +
						"; max-age=" + 60 * 60 * 24 * lifespanInDays + "; path=/" + domainString ;
}

function getCookie(cookieName) {
	var cookieString = document.cookie ;
	if (cookieString.length != 0) {
	var cookieValue = cookieString.match ('(^|;)[\s]*' + cookieName + '=([^;]*)' );
        return decodeURIComponent ( cookieValue[2] ) ;
    }
    return false;
}

function deleteCookie(cookieName, validDomain) {
    var domainString = validDomain ? ("; domain=" + validDomain) : '' ;
    document.cookie = cookieName + "=; max-age=0; path=/" + domainString ;
}

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    return JSON.parse(this.getItem(key));
};