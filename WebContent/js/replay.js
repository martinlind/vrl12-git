function replay() {	
	this.player1 = new grid(25.5, 25.5, true);			
	this.player2 = new grid(325.5, 25.5, true);
	
	this.squareWidth = 25;
	this.squareHeight = 25;
	this.shots = [];
	this.state = 0;
	var autoReplayOn = false;
	this.explosions = [];
	this.splashes = [];
	this.clearShipLayer = true;
}

function replayInfo(id, player1, player2) {
	this.id = id;
	this.player1 = player1;
	this.player2 = player2;
}

replay.prototype.loadSteps = function() {
	$('#steps').empty();
	for (var i = 0; i < this.shots.length; i++) {
		var shot = this.shots[i];
		var isPlayer1Shot = shot.isPlayer1Shot;
		var x = shot.x;
		var y = shot.y;
		$('#steps').append('<div id="step'+(i+1)+'" class="singlestep" onclick="toState('+(i+1)+');">'+(i+1)+'. '+(isPlayer1Shot ? 'P1' : 'P2')+': ('+(x+1)+', '+(y+1)+')</div>');
	}
};

replay.prototype.draw = function() {
	var ctx = document.getElementById('canvas2').getContext('2d');	
	ctx.clearRect(0, 0, 600, 300);
	
	this.nextFrame();

	this.player1.draw(ctx);
	this.player2.draw(ctx);
	
	for (var k in this.explosions) {
		this.explosions[k].draw(ctx);
	}
			
	for (var l in this.splashes) {
			this.splashes[l].draw(ctx);
	}
};

replay.prototype.drawGrid = function() {
	var ctxg = document.getElementById('replaygridlayer').getContext('2d');	
	ctxg.clearRect(0, 0, ctxg.canvas.width, ctxg.canvas.height);

	this.player1.drawGrid(ctxg);
	this.player2.drawGrid(ctxg);
};

replay.prototype.drawShips = function() {	
	var ctxs = document.getElementById('replayshiplayer').getContext('2d');	
	if (this.clearShipLayer) {
		ctxs.clearRect(0, 0, ctxs.canvas.width, ctxs.canvas.height);
		this.clearShipLayer = false;
	}
	this.player1.drawFleet(ctxs);
	this.player2.drawFleet(ctxs);
	this.nextShipFrame();
};

replay.prototype.drawExplosions = function() {
	var ctxe = document.getElementById('replayexplosionlayer').getContext('2d');	
	this.nextExplosionFrame();
	ctxe.clearRect(0, 0, ctxe.canvas.width, ctxe.canvas.height);
	for (var k = 0; k < this.explosions.length; k++) {
		this.explosions[k].draw(ctxe);
	}
			
	for (var l = 0; l < this.splashes.length; l++) {
		this.splashes[l].draw(ctxe);
	}
};

replay.prototype.drawMissiles = function() {
	var ctxm = document.getElementById('replaymissilelayer').getContext('2d');	
	this.nextMissileFrame();
	ctxm.clearRect(0, 0, ctxm.canvas.width, ctxm.canvas.height);
	this.player1.drawMissiles(ctxm);
	this.player2.drawMissiles(ctxm);
};

replay.prototype.jump = function(step, relative) {
	//features
	//relative jump: goto state + step
	//absolute jump: goto step
	//jump to end: goto shot.length
	//jump to start: goto 0
	var targetState =  relative ? this.state + step : step;
	console.log("target state: " + targetState);
	var endState = this.shots.length;
	console.log("endstate: " + endState);
	var forward = ((targetState - this.state) < 0) ? false : true;
	while (true) {
		var nextState = this.state + (forward ? 1 : -1);
		if (this.state == endState) this.autoReplayOn = false;
		if ((nextState < 0) || (nextState > endState) || (this.state == targetState)) {
			break;
		}
		else this.state = nextState;
		this.processSingleState(forward);
		if (this.state == endState) this.autoReplayOn = false;
	}
	console.log("displaying state: " + this.state);
};

replay.prototype.jumpa = function(step) {
	//features
	//relative jump: goto state + step
	//absolute jump: goto step
	//jump to end: goto shot.length
	//jump to start: goto 0
	var targetState =  step;
	console.log("target state: " + targetState);
	var endState = this.shots.length;
	console.log("endstate: " + endState);	
	//if (this.state == endState) this.autoReplayOn = false;
	var forward = ((targetState - this.state) < 0) ? false : true;
	var nextState = this.state + (forward ? 1 : -1);
	updateStepHighlight();
	scrollReplayStep();
	if ((((nextState >= 0) && !forward) || ((nextState <= endState) && forward)) && (this.state != targetState)) {		
		this.state = nextState;
		this.processSingleState(forward);
		console.log("displaying state: " + this.state);
		jumpInterval = setTimeout('activeReplay.jumpa('+step+');', 200);
	}
};

replay.prototype.processSingleState = function(forward) {
	console.log("processing state: " + this.state);
	var activeShot = this.shots[this.state-1*forward];
	var activePlayer = activeShot.isPlayer1Shot ? this.player2 : this.player1;
	activePlayer.cells[activeShot.y][activeShot.x].bomb = forward;
	if (!forward) activePlayer.cells[activeShot.y][activeShot.x].missile = forward;
	if (forward) activePlayer.missiles.push(new missile(activeShot.x, activeShot.y));
	var shipIndex = getShipIndexBySquare(activePlayer.fleet, activeShot.x, activeShot.y);
	if (shipIndex != -1) {
		activePlayer.fleet[shipIndex].changed = true;
		var shipDamage = getDamage(activePlayer.fleet[shipIndex], activePlayer.cells);
		activePlayer.fleet[shipIndex].damage = shipDamage;
		if (!animateCanvas || !forward) activePlayer.fleet[shipIndex].visualDamage = shipDamage;
		if (shipDamage == Math.pow(2, activePlayer.fleet[shipIndex].length)-1) {
			activePlayer.fleet[shipIndex].sunk = true;
		} else {
			activePlayer.fleet[shipIndex].sunk = false;
		}
		if (!forward) activePlayer.fleet[shipIndex].visibility = 0;
	} else if (!forward) activePlayer.missiles.pop();
};

function loadReplay(rid, replayElementId) {
	console.log('load replay: '+rid);
	updateReplayHighlight(replayElementId);
	var lightReplay = localStorage.getObject(rid);
	activeReplay = new replay();
	activeReplay.player1.fleet = lightReplay.fleet1;
	activeReplay.player1.resetReplayGrid();
	activeReplay.player2.fleet = lightReplay.fleet2;
	activeReplay.player2.resetReplayGrid();
	activeReplay.shots = lightReplay.shots;
	activeReplay.loadSteps();
	//activeReplay.draw();
	updateReplayBoxes();
	$('.replaybutton').fadeIn('slow');
	activeReplay.drawGrid();
}

grid.prototype.resetReplayGrid = function() {
	var fleet = this.fleet;
	var cells = this.cells;
	//reset ships
	for (var i = 0; i < fleet.length; i++) {
		var shipo = fleet[i];
		fleet[i] = new ship(shipo.x, shipo.y, shipo.vertical, shipo.length, 0, false, "#808080", "#000000");
	}
	//update ship property
	for(var i=0;i<10;i++) {
		for(var j=0;j<10;j++) {
			var isShip = getShipIndexBySquare(fleet, j, i) > -1;
			cells[i][j].ship = isShip;
		}
	}
	this.fleet = fleet;
	this.cells = cells;
};

function loadReplays() {
	$('#replays').empty();
	var replays = localStorage.getObject('replays');
	if (replays) {
		var i = 0;
		$.each(replays, function(replayId, replayInfo) {
			$('#replays').append('<div id="replay'+i+'" class="singlereplay" onclick="loadReplay(\''+replayId+'\', \'replay'+i+'\')">'+(i+1)+'. '+replayInfo.player1.name+' vs '+replayInfo.player2.name+'</div>');
			i++;
		});
	}
	var replayCount = $('#replays div').length;
	$('#replaybutton').html('Replay' + (replayCount > 0 ? ' ('+replayCount+')' : ''));
	updateReplayBoxes();
}

function updateReplayBoxes() {
	if ($('#replays div').length == 0) $('#replaysbox').hide();
	else $('#replaysbox').slideDown();;
	if ($('#steps div').length == 0) $('#stepsbox').hide();
	else $('#stepsbox').show('fast');
}

function lightReplay(fleet1, fleet2, shots) {
	this.fleet1 = fleet1;
	this.fleet2 = fleet2;
	this.shots = shots;
}

function shot(isPlayer1Shot, x, y) {
	this.isPlayer1Shot = isPlayer1Shot;
	this.x = x;
	this.y = y;
}