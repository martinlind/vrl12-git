var missile_canvas;
var shipImages = {};

/**
 * Ship object
 * @param x nose square index
 * @param y nose square index
 * @param vertical orientation
 * @param length ship length
 * @param damage damage value, decimal reprecenting binary damages
 * @param sunk marks ship as sunk
 * @param fill ship color
 * @param stroke outline color
 * @returns
 */
function ship(x, y, vertical, length, damage, sunk, fill, stroke) {
	this.x = x;
	this.y = y;
	this.vertical = vertical;
	this.length = length;
	this.damage = damage;
	this.sunk = sunk;
	this.fill = fill;
	this.stroke = stroke;
	this.visibility = 0;
	this.visualDamage = 0;
	this.opacity = 0;
	this.changed = true;
}

/**
 * Draws the ship
 * @param ctx canvas context
 */
ship.prototype.draw = function(ctx) {
	drawShip(ctx, this.length, this.x, this.y, this.vertical, this.visualDamage, this.sunk, this.fill, this.stroke, this.visibility, this.opacity);
};

function missile(x, y) {
	this.x = x;
	this.y = y;
	this.height = 1000;
}

missile.prototype.draw = function(ctx) {
	drawMissile(ctx, this.x, this.y, this.height);
};

function preRender() {
	console.log("prerendering");
	missile_canvas = document.createElement('canvas');
	
	missile125_canvas = document.createElement('canvas');
	missile100_canvas = document.createElement('canvas');
	missile75_canvas = document.createElement('canvas');
	missile50_canvas = document.createElement('canvas');
	missile25_canvas = document.createElement('canvas');
	
	missile_canvas.width = 125;
	missile_canvas.height = 125;
	
	missile125_canvas.width = 125;
	missile125_canvas.height = 125;
	missile100_canvas.width = 100;
	missile100_canvas.height = 100;
	missile75_canvas.width = 75;
	missile75_canvas.height = 75;
	missile50_canvas.width = 50;
	missile50_canvas.height = 50;
	missile25_canvas.width = 25;
	missile25_canvas.height = 25;
	
	var missile_context = missile_canvas.getContext('2d');
	drawMissile(missile_context,0,0,1000);
	
	var missile125_context = missile125_canvas.getContext('2d');
	drawMissile(missile125_context,0,0,1000);
	var missile100_context = missile100_canvas.getContext('2d');
	drawMissile(missile100_context,0,0,750);
	var missile75_context = missile75_canvas.getContext('2d');
	drawMissile(missile75_context,0,0,500);
	var missile50_context = missile50_canvas.getContext('2d');
	drawMissile(missile50_context,0,0,250);
	var missile25_context = missile25_canvas.getContext('2d');
	drawMissile(missile25_context,0,0,0);
	
	var maxShipLength = 4;
	
	for (var i = 0; i < maxShipLength; i++) {
		for (var d = 0; d < Math.pow(2, i+1); d++) {
			for (var v = 0; v < 2; v++) {
				for (var c = 0; c < 2; c++) {
					var shipImage = document.createElement('canvas');
					var shipImageName = 'ship'+(i+1)+'d'+d+'v'+v+'c'+c;
					shipImage.width = v ? 25 : 25*(i+1);
					shipImage.height = v ? 25*(i+1) : 25;
					var shipContext = shipImage.getContext('2d');
					drawShip(shipContext,0,0,1000);
					var fill = c ? "#00FF00" : "#808080";
					var stroke = c ?  "#00FF00" : "#000000";
					drawShip(shipContext, (i+1), 0, 0, v, d, false, fill, stroke, 0, 1);
					shipImages[shipImageName] = shipImage;
				}
			}
		}
	}
}

function drawMissileImg(ctx, x, y, height) {
	ctx.save();
	var size = 0.2;
	//var missilecanvas = missile25_canvas;
	
	if (height > 750) {
		size = 0.2 + 0.0008 * height;
		ctx.drawImage(missile125_canvas, x, y, 125*size, 125*size);
	} else if (height > 500) {
		size = 0.2 + 0.0008 * height;
		ctx.drawImage(missile100_canvas, x, y, 125*size, 125*size);
	} else if (height > 250) {
		size = 0.2 + 0.0008 * height;
		ctx.drawImage(missile75_canvas, x, y, 125*size, 125*size);
	} else if (height > 0) {
		size = 0.2 + 0.0008 * height;		
		ctx.drawImage(missile50_canvas, x, y, 125*size, 125*size);
	} else ctx.drawImage(missile25_canvas, x, y, 125*size, 125*size);
	/*
	if (height > 500) {
		size = 0.2 + 0.0008 * height;
	} 
	ctx.drawImage(missile100_canvas, x, y, 125*size, 125*size);
	*/
	if (height <= 0) {
		var visibility = height / -200;
		ctx.fillStyle = "rgba(0,160,255,"+visibility+")";
		ctx.fillRect(x,y,25,25);
	}
	ctx.restore();
}

function drawShipImg(ctx, squares, x, y, vertical, damage, sunk, fill, stroke, visibility, opacity) {
	var c = (fill == "#808080") ? 0 : 1;
	var shipImageName = 'ship'+squares+'d'+damage+'v'+(vertical ? 1 : 0)+'c'+c;
	ctx.save();
	ctx.globalAlpha = opacity;
	ctx.drawImage(shipImages[shipImageName], x, y);
	if (damage == Math.pow(2, squares) - 1) {
		ctx.save();
		ctx.translate(x,y);
		if (!vertical) {
			ctx.translate(0,25);
			ctx.rotate(-Math.PI/2);
		}
		ctx.fillStyle = "rgba(0,160,255,"+visibility+")";
  		ctx.fillRect(0,0,25,25*squares);
		ctx.restore();
	};
	ctx.restore();
}

/**
 * Draws a missile
 * @param ctx canvas context
 * @param x vertical coordinate on canvas
 * @param y horizontal  coordinate on canvas
 * @param height height of the missile from water, normal height would be from 1000 to -160
 */
function drawMissile(ctx, x, y, height) {
	ctx.save();
	ctx.translate(x,y);
	var size = 1;
	if (height > 0) {
		size = 1 + height / 250;
		ctx.scale(size,size);
	}
	ctx.fillStyle = "#808080";
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = 10;
	ctx.save();
	ctx.transform(0.0276,0,0,0.0276,0,0);
	ctx.beginPath();
	ctx.moveTo(270,591);
	ctx.bezierCurveTo(251,572,201,520,174,493);
	ctx.bezierCurveTo(54,326,196,140,363,172);
	ctx.bezierCurveTo(434,196,440,197,488,214);
	ctx.fill();
	ctx.stroke();
	
	ctx.save();
	ctx.transform(0.95,0,0,0.96,-4.4,-47.52);
	ctx.beginPath();
	ctx.moveTo(670,488);
	ctx.translate(439,488);
	ctx.arc(0,0,231,-0.0002,3.14,0);
	ctx.translate(-439,-488);
	ctx.translate(439,488);
	ctx.arc(0,0,231,3.14,6.28,0);
	ctx.translate(-439,-488);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	ctx.restore();

	ctx.beginPath();
	ctx.moveTo(825,266);
	ctx.lineTo(923,296);
	ctx.lineTo(585,437);
	ctx.lineTo(521,401);
	ctx.lineTo(615,331);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(460,384);
	ctx.lineTo(320,49);
	ctx.lineTo(273,39);
	ctx.lineTo(327,221);
	ctx.lineTo(399,353);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(350,469);
	ctx.lineTo(410,513);
	ctx.lineTo(73,653);
	ctx.lineTo(37,601);
	ctx.lineTo(207,501);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(472,518);
	ctx.lineTo(531,562);
	ctx.lineTo(670,891);
	ctx.lineTo(592,827);
	ctx.lineTo(492,626);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
	
	ctx.restore();
	if (height < 0) {
		var visibility = height / -200;
		ctx.fillStyle = "rgba(0,160,255,"+visibility+")";
		ctx.fillRect(0,0,25,25);
	}
	ctx.restore();
}

/**
 * Draws a ship
 * @param ctx canvas context
 * @param squares ship lenght
 * @param x vertical coordinate on canvas
 * @param y horizontal  coordinate on canvas
 * @param vertical orientation
 * @param damage damage value, decimal reprecenting binary damages
 * @param sunk marks ship as sunk
 * @param fill ship color
 * @param stroke outline color
 */
function drawShip(ctx, squares, x, y, vertical, damage, sunk, fill, stroke, visibility, opacity) {
	ctx.save();
	ctx.globalAlpha = opacity;
	switch(squares) {
	case 1:
		switch(damage) {
		case 0:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(12.5,1);
			ctx.bezierCurveTo(7,6,7,20,8,24);
			ctx.lineTo(17,24);
			ctx.bezierCurveTo(18,20,18,6,12.5,1);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		case 1:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(12.5,1);
			ctx.bezierCurveTo(11,2.3,10,4.3,9.2,6.6);
			ctx.lineTo(12,9);
			ctx.lineTo(8.25,10.25);
			ctx.bezierCurveTo(7.2,15.7,7.4,21.6,8,24);
			ctx.lineTo(17,24);
			ctx.bezierCurveTo(17.2,23.1,17.4,21.7,17.4,20);
			ctx.lineTo(13,18);
			ctx.lineTo(17.4,14.7);
			ctx.bezierCurveTo(17,9.6,15.6,3.8,12.5,1);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		}
		break;
	case 2:
		switch(damage) {
		case 0:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(12.504838,2);
			ctx.bezierCurveTo(2,6,0,40,4,48);
			ctx.lineTo(21,48);
			ctx.bezierCurveTo(25,40,23,6,12.5,2);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		case 1:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(14.7,3.3);
			ctx.lineTo(12.1,5.9);
			ctx.lineTo(9.6,4);
			ctx.bezierCurveTo(8.2,5.6,6.9,7.7,5.9,10.3);
			ctx.lineTo(9,14);
			ctx.lineTo(3.4,19.6);
			ctx.bezierCurveTo(1.4,30.7,1.7,43.6,4,48);
			ctx.lineTo(21,48);
			ctx.bezierCurveTo(23,44,23.5,33.3,22.1,23.2);
			ctx.lineTo(18.3,22.4);
			ctx.lineTo(21.4,18.7);
			ctx.bezierCurveTo(21.2,17.9,21.1,17.3,20.9,16.6);
			ctx.lineTo(16.2,14.8);
			ctx.lineTo(19.25,10.8);
			ctx.bezierCurveTo(18,7.6,16.6,4.9,14.7,3.3);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		case 2:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(12.5,2);
			ctx.bezierCurveTo(6.3,4.3,3,17.2,2.3,29);
			ctx.lineTo(6,31.4);
			ctx.lineTo(2.1,34.5);
			ctx.bezierCurveTo(2.1,35,2.1,35.6,2.1,36.2);
			ctx.lineTo(5.8,39.7);
			ctx.lineTo(2.5,41.6);
			ctx.bezierCurveTo(2.8,44.4,3.3,46.6,4,48);
			ctx.lineTo(5.8,48);
			ctx.lineTo(12.75,43.6);
			ctx.lineTo(15,48);
			ctx.lineTo(21,48);
			ctx.bezierCurveTo(21.8,46.5,22.3,43.9,22.6,40.8);
			ctx.lineTo(17.7,34.6);
			ctx.lineTo(22.8,31.9);
			ctx.bezierCurveTo(22.4,19.4,19.2,4.5,12.5,2);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		case 3:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.beginPath();
			ctx.moveTo(14.7,3.3);
			ctx.lineTo(12.1,5.9);
			ctx.lineTo(9.6,4);
			ctx.bezierCurveTo(8.2,5.6,7,7.75,6,10.3);
			ctx.lineTo(9,14);
			ctx.lineTo(3.4,19.6);
			ctx.bezierCurveTo(2.9,22.7,2.5,25.9,2.3,29);
			ctx.lineTo(6,31.4);
			ctx.lineTo(2.1,34.5);
			ctx.bezierCurveTo(2.1,35,2.1,35.6,2.1,36.2);
			ctx.lineTo(5.8,39.7);
			ctx.lineTo(2.5,41.7);
			ctx.bezierCurveTo(2.8,44.4,3.3,46.6,4,48);
			ctx.lineTo(5.8,48);
			ctx.lineTo(12.75,43.6);
			ctx.lineTo(15,48);
			ctx.lineTo(21,48);
			ctx.bezierCurveTo(21.8,46.5,22.3,43.9,22.6,40.8);
			ctx.lineTo(17.7,34.6);
			ctx.lineTo(22.8,31.9);
			ctx.bezierCurveTo(22.7,29,22.5,26,22.1,23.2);
			ctx.lineTo(18.3,22.4);
			ctx.lineTo(21.4,18.7);
			ctx.bezierCurveTo(21.2,18,21,17.2,20.9,16.6);
			ctx.lineTo(16.2,14.8);
			ctx.lineTo(19.3,10.9);
			ctx.bezierCurveTo(18,7.6,16.6,5,14.7,3.3);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			break;
		}
		break;
	case 3:
		switch(damage) {
		case 0:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.beginPath();
			ctx.moveTo(12.504838,979.39372);
			ctx.bezierCurveTo(2,985.36217,0,1038.3622,4,1050.3622);
			ctx.lineTo(21,1050.3622);
			ctx.bezierCurveTo(25,1038.3622,23,985.36217,12.504838,979.39372);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 1:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(12.283338,2.1543493,12.084449,2.3064095,11.875,2.46875);
			ctx.lineTo(13.5,5.09375);
			ctx.lineTo(10,4.53125);
			ctx.bezierCurveTo(9.4458309,5.3450591,8.9304226,6.3176487,8.4375,7.40625);
			ctx.lineTo(9.96875,10.75);
			ctx.lineTo(6.75,12.03125);
			ctx.bezierCurveTo(6.3781429,13.254892,6.0128708,14.56597,5.6875,15.9375);
			ctx.lineTo(7.9375,18.34375);
			ctx.lineTo(4.8125,20.0625);
			ctx.bezierCurveTo(1.33526,38.439233,1.315585,64.946755,4,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(23.971788,64.084635,23.612584,32.545295,18.9375,14.46875);
			ctx.lineTo(16.15625,13.65625);
			ctx.lineTo(18.0625,11.34375);
			ctx.bezierCurveTo(16.577365,6.6896423,14.724974,3.2965611,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 2:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(8.1412461,4.5077284,5.2654619,15.086499,3.65625,27.46875);
			ctx.lineTo(9.96875,30.6875);
			ctx.lineTo(2.9375,34.0625);
			ctx.bezierCurveTo(2.892683,34.554635,2.8223809,35.037364,2.78125,35.53125);
			ctx.lineTo(5.6875,39.53125);
			ctx.lineTo(2.40625,41.4375);
			ctx.bezierCurveTo(1.6790117,54.886757,2.2649175,67.794752,4,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(22.398042,68.805875,23.05015,59.577556,22.875,49.125);
			ctx.lineTo(19.5625,48.25);
			ctx.lineTo(22.75,44.78125);
			ctx.bezierCurveTo(22.71834,43.947644,22.69828,43.121969,22.65625,42.28125);
			ctx.lineTo(19.3125,40.40625);
			ctx.lineTo(22.4375,38.71875);
			ctx.bezierCurveTo(22.40813,38.2822,22.37595,37.842655,22.34375,37.40625);
			ctx.lineTo(18.9375,35.875);
			ctx.lineTo(22,33.34375);
			ctx.bezierCurveTo(20.596894,18.567426,17.530229,4.89187,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 3:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(12.283338,2.1543493,12.084449,2.3064095,11.875,2.46875);
			ctx.lineTo(13.5,5.09375);
			ctx.lineTo(10,4.53125);
			ctx.bezierCurveTo(9.4458309,5.3450591,8.9304226,6.3176487,8.4375,7.40625);
			ctx.lineTo(9.96875,10.75);
			ctx.lineTo(6.75,12.03125);
			ctx.bezierCurveTo(6.3781429,13.254892,6.0128708,14.56597,5.6875,15.9375);
			ctx.lineTo(7.9375,18.34375);
			ctx.lineTo(4.8125,20.0625);
			ctx.bezierCurveTo(4.3688521,22.407118,3.9576466,24.883386,3.625,27.4375);
			ctx.lineTo(9.96875,30.6875);
			ctx.lineTo(2.9375,34.0625);
			ctx.bezierCurveTo(2.891908,34.562244,2.8542899999999998,35.060951,2.8125,35.5625);
			ctx.lineTo(5.6875,39.53125);
			ctx.lineTo(2.375,41.4375);
			ctx.bezierCurveTo(1.6482522,54.879028,2.2654415,67.796325,4,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(22.398569,68.804293,23.054575,59.580795,22.875,49.125);
			ctx.lineTo(19.5625,48.25);
			ctx.lineTo(22.75,44.78125);
			ctx.bezierCurveTo(22.7184,43.956748,22.698,43.143982,22.65625,42.3125);
			ctx.lineTo(19.3125,40.40625);
			ctx.lineTo(22.4375,38.71875);
			ctx.bezierCurveTo(22.40798,38.282381,22.37609,37.842472,22.34375,37.40625);
			ctx.lineTo(18.9375,35.875);
			ctx.lineTo(22,33.34375);
			ctx.bezierCurveTo(21.347549,26.492104,20.336155,19.876748,18.9375,14.46875);
			ctx.lineTo(16.15625,13.65625);
			ctx.lineTo(18.0625,11.34375);
			ctx.bezierCurveTo(16.577365,6.6896423,14.724974,3.2965611,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 4:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(4.2290966,6.7304632,1.2602558,40.519163,2.3125,60.5625);
			ctx.lineTo(6.4375,60.09375);
			ctx.lineTo(5.4375,67.9375);
			ctx.lineTo(10.96875,65.78125);
			ctx.lineTo(11.0625,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(21.209818,72.370547,21.38606,71.592368,21.5625,70.75);
			ctx.lineTo(19.8125,68.5625);
			ctx.lineTo(22.28125,66.28125);
			ctx.bezierCurveTo(22.345429,65.704248,22.382657,65.118328,22.4375,64.5);
			ctx.lineTo(16.90625,59.71875);
			ctx.lineTo(22.84375,57.15625);
			ctx.bezierCurveTo(23.409079,36.902434,20.305005,6.469846,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 5:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(12.283338,2.1543493,12.084449,2.3064095,11.875,2.46875);
			ctx.lineTo(13.5,5.09375);
			ctx.lineTo(10,4.53125);
			ctx.bezierCurveTo(9.4458309,5.3450591,8.9304226,6.3176487,8.4375,7.40625);
			ctx.lineTo(9.96875,10.75);
			ctx.lineTo(6.75,12.03125);
			ctx.bezierCurveTo(6.3781429,13.254892,6.0128708,14.56597,5.6875,15.9375);
			ctx.lineTo(7.9375,18.34375);
			ctx.lineTo(4.8125,20.0625);
			ctx.bezierCurveTo(2.4404834,32.59828,1.7058379000000001,48.898553,2.3125,60.5625);
			ctx.lineTo(6.4375,60.09375);
			ctx.lineTo(5.4375,67.9375);
			ctx.lineTo(10.96875,65.78125);
			ctx.lineTo(11.0625,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(21.209936,72.370193,21.417408000000002,71.592891,21.59375,70.75);
			ctx.lineTo(19.8125,68.5625);
			ctx.lineTo(22.25,66.28125);
			ctx.bezierCurveTo(22.31297,65.714148,22.383586,65.107108,22.4375,64.5);
			ctx.lineTo(16.90625,59.71875);
			ctx.lineTo(22.84375,57.15625);
			ctx.bezierCurveTo(23.207794,43.905479,21.999461,26.308039,18.9375,14.46875);
			ctx.lineTo(16.15625,13.65625);
			ctx.lineTo(18.0625,11.34375);
			ctx.bezierCurveTo(16.577365,6.6896423,14.724974,3.2965611,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 6:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(8.1412461,4.5077284,5.2654619,15.086499,3.65625,27.46875);
			ctx.lineTo(9.96875,30.6875);
			ctx.lineTo(2.9375,34.0625);
			ctx.bezierCurveTo(2.892683,34.554635,2.8223809,35.037364,2.78125,35.53125);
			ctx.lineTo(5.6875,39.53125);
			ctx.lineTo(2.40625,41.4375);
			ctx.bezierCurveTo(2.0369272,48.26761,1.9947171,54.949553,2.28125,60.5625);
			ctx.lineTo(6.4375,60.09375);
			ctx.lineTo(5.4375,67.9375);
			ctx.lineTo(10.96875,65.78125);
			ctx.lineTo(11.0625,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(21.209404,72.371787,21.418089,71.591297,21.59375,70.75);
			ctx.lineTo(19.8125,68.5625);
			ctx.lineTo(22.25,66.28125);
			ctx.bezierCurveTo(22.31244,65.71582,22.384033,65.105154,22.4375,64.5);
			ctx.lineTo(16.90625,59.71875);
			ctx.lineTo(22.8125,57.15625);
			ctx.bezierCurveTo(22.88207,54.613231999999996,22.921749,51.914896999999996,22.875,49.125);
			ctx.lineTo(19.5625,48.25);
			ctx.lineTo(22.75,44.78125);
			ctx.bezierCurveTo(22.71834,43.947644,22.69828,43.121969,22.65625,42.28125);
			ctx.lineTo(19.3125,40.40625);
			ctx.lineTo(22.4375,38.71875);
			ctx.bezierCurveTo(22.40813,38.2822,22.37595,37.842655,22.34375,37.40625);
			ctx.lineTo(18.9375,35.875);
			ctx.lineTo(22,33.34375);
			ctx.bezierCurveTo(20.596894,18.567426,17.530229,4.89187,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 7:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,75);
			ctx.lineTo(0,75);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-977.36217);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,977.36217);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(12.283338,2.1543493,12.084449,2.3064095,11.875,2.46875);
			ctx.lineTo(13.5,5.09375);
			ctx.lineTo(10,4.53125);
			ctx.bezierCurveTo(9.4458309,5.3450591,8.9304226,6.3176487,8.4375,7.40625);
			ctx.lineTo(9.96875,10.75);
			ctx.lineTo(6.75,12.03125);
			ctx.bezierCurveTo(6.3781429,13.254892,6.0128708,14.56597,5.6875,15.9375);
			ctx.lineTo(7.9375,18.34375);
			ctx.lineTo(4.8125,20.0625);
			ctx.bezierCurveTo(4.3688521,22.407118,3.9576466,24.883386,3.625,27.4375);
			ctx.lineTo(9.96875,30.6875);
			ctx.lineTo(2.9375,34.0625);
			ctx.bezierCurveTo(2.891908,34.562244,2.8542899999999998,35.060951,2.8125,35.5625);
			ctx.lineTo(5.6875,39.53125);
			ctx.lineTo(2.375,41.4375);
			ctx.bezierCurveTo(2.0058818,48.264507,1.986672,54.947932,2.28125,60.5625);
			ctx.lineTo(6.4375,60.09375);
			ctx.lineTo(5.4375,67.9375);
			ctx.lineTo(10.96875,65.78125);
			ctx.lineTo(11.0625,73);
			ctx.lineTo(21,73);
			ctx.bezierCurveTo(21.209483,72.37155,21.417898,71.591295,21.59375,70.75);
			ctx.lineTo(19.8125,68.5625);
			ctx.lineTo(22.25,66.28125);
			ctx.bezierCurveTo(22.31255,65.715911,22.383944,65.105059,22.4375,64.5);
			ctx.lineTo(16.90625,59.71875);
			ctx.lineTo(22.8125,57.15625);
			ctx.bezierCurveTo(22.88186,54.613241,22.92293,51.915762,22.875,49.125);
			ctx.lineTo(19.5625,48.25);
			ctx.lineTo(22.75,44.78125);
			ctx.bezierCurveTo(22.7184,43.956748,22.698,43.143982,22.65625,42.3125);
			ctx.lineTo(19.3125,40.40625);
			ctx.lineTo(22.4375,38.71875);
			ctx.bezierCurveTo(22.40798,38.282381,22.37609,37.842472,22.34375,37.40625);
			ctx.lineTo(18.9375,35.875);
			ctx.lineTo(22,33.34375);
			ctx.bezierCurveTo(21.347549,26.492104,20.336155,19.876748,18.9375,14.46875);
			ctx.lineTo(16.15625,13.65625);
			ctx.lineTo(18.0625,11.34375);
			ctx.bezierCurveTo(16.577365,6.6896423,14.724974,3.2965611,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		}
		break;
	case 4:
		switch(damage) {
		case 0:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.beginPath();
			ctx.moveTo(12.49084,954.40682);
			ctx.bezierCurveTo(0,954.3622,0,1015.3622,2,1050.3622);
			ctx.lineTo(23,1050.3622);
			ctx.bezierCurveTo(25,1015.3622,25,954.3622,12.49084,954.40682);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 1:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.3888962,2.020137,7.0671819,5.8133654,5.34375,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.0921957,17.43567,3.9200785,18.401641,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.15625,23.0625);
			ctx.bezierCurveTo(0.2681933,43.9969,0.74813053,76.092284,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(24.292082,75.388563,24.742308,41.93788,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.09375,10.1875);
			ctx.bezierCurveTo(17.431396,5.085597,15.291283,2.0212935,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 2:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(7.0003512,2.0116041,3.8959156,13.843722,2.34375,30.0625);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(0.6782691,63.747933,1.1535028,83.186299,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.80129,83.977419,24.264859,65.805409,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.31366,38.840998,23.145414,35.802447,22.90625,32.90625);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.34375,27.09375);
			ctx.bezierCurveTo(20.706108,12.421265,17.665272,2.0128255,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 3:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.3888962,2.020137,7.0671819,5.8133654,5.34375,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.0921957,17.43567,3.9200785,18.401641,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.15625,23.0625);
			ctx.bezierCurveTo(2.8518448,25.269015,2.5763619,27.632998,2.34375,30.0625);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.3125,46.25);
			ctx.bezierCurveTo(0.6535724,63.765222,1.1543869,83.20177,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.801115,83.980494,24.268151,65.796175,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.311786,38.820036,23.117183,35.789119,22.875,32.875);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.3125,27.09375);
			ctx.bezierCurveTo(22.079603,25.015938,21.822147,22.98207,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.09375,10.1875);
			ctx.bezierCurveTo(17.431396,5.085597,15.291283,2.0212935,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 4:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(4.2309592,2.0017112,1.4581343,28.746186,1.09375,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.125,74.25);
			ctx.bezierCurveTo(1.2787984,82.75017,1.5948614,90.910074,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.541016,88.532227,23.927475,77.145039,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.96875,59.9375);
			ctx.bezierCurveTo(23.966575,59.623078,23.940253,59.314237,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.354412,26.878786,20.48686,2.002761,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 5:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.3886859,2.020136,7.0320728,5.8089244,5.3125,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.3125,16.5);
			ctx.bezierCurveTo(4.1224713,17.44212,3.9207768,18.394178,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.1875,23.0625);
			ctx.bezierCurveTo(1.8621067,32.699984,1.2523293,44.686352,1.09375,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.125,74.25);
			ctx.bezierCurveTo(1.2787984,82.75017,1.5948614,90.910074,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.541016,88.532227,23.927475,77.145039,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.96875,59.9375);
			ctx.bezierCurveTo(23.966575,59.623078,23.940253,59.314237,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.64289,41.890378,22.942201,30.280739,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.09375,10.15625);
			ctx.bezierCurveTo(17.435174,5.0584114,15.289085,2.0213014,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 6:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(7.000368,2.0116041,3.9518895,13.868857,2.40625,30.09375);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(1.2109346,49.74141,1.1405532,53.343715,1.09375,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.125,74.25);
			ctx.bezierCurveTo(1.2787984,82.75017,1.5948614,90.910074,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.541016,88.532227,23.927475,77.145039,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.96875,59.9375);
			ctx.bezierCurveTo(23.966575,59.623078,23.940253,59.314237,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.84327,52.317407,23.7998,50.661,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.313425,38.827688,23.11487,35.782803,22.875,32.875);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.3125,27.09375);
			ctx.bezierCurveTo(20.679111,12.42751,17.662981,2.0128337,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 7:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.386514,2.0201279,7.0581168,5.8230569,5.34375,12.0625);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.0921568,17.441835,3.9512476,18.426091,3.78125,19.4375);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.21875,23.0625);
			ctx.bezierCurveTo(2.9128705,25.291014,2.6400652,27.639344,2.40625,30.09375);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(1.2109346,49.74141,1.1405532,53.343715,1.09375,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.125,74.25);
			ctx.bezierCurveTo(1.2787984,82.75017,1.5948614,90.910074,2,98);
			ctx.lineTo(23,98);
			ctx.bezierCurveTo(23.541016,88.532227,23.927475,77.145039,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.96875,59.9375);
			ctx.bezierCurveTo(23.966575,59.623078,23.940253,59.314237,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.84327,52.317407,23.7998,50.661,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.313425,38.827688,23.11487,35.782803,22.875,32.875);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.3125,27.09375);
			ctx.bezierCurveTo(22.078508,24.992728,21.792992,23.002096,21.5,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.09375,10.1875);
			ctx.bezierCurveTo(17.437338,5.0844231,15.288556,2.0213032,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 8:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(1.7712788,1.9929247,0.25866137,46.968332,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(24.644385,44.237465,22.912578,1.9941085,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 9:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.389682,2.020139,7.0669469,5.8188299,5.34375,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.091967,17.437376,3.9202414,18.399735,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.1875,23.0625);
			ctx.bezierCurveTo(0.95220237,39.271808,0.71776279,62.161696,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(24.277233,58.851617000000005,23.903699,36.525591,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.125,10.125);
			ctx.bezierCurveTo(17.465761,5.0484379,15.284125,2.0213191,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 10:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(6.9968252,2.0115915,3.9572188,13.864639,2.40625,30.09375);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(0.90199638,57.88995,0.96671249,70.400362,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(24.022588,68.987505,24.04744,58.774072,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.313703,38.83089,23.114656,35.81123,22.875,32.90625);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.34375,27.09375);
			ctx.bezierCurveTo(20.708543,12.413682,17.668369,2.0128145,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 11:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.3842319,2.0201198,7.0570371,5.822475,5.34375,12.0625);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.094283,17.43325,3.9495744,18.436021,3.78125,19.4375);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.21875,23.0625);
			ctx.bezierCurveTo(2.9126221,25.29131,2.6408714,27.638706,2.40625,30.09375);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(0.90199638,57.88995,0.96671249,70.400362,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(24.022588,68.987505,24.04744,58.774072,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.313703,38.83089,23.114656,35.81123,22.875,32.90625);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.34375,27.09375);
			ctx.bezierCurveTo(22.1113,25.006933,21.853809,23.021236,21.5625,21.125);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.125,10.125);
			ctx.bezierCurveTo(17.463656,5.0576738,15.28112,2.0213298,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 12:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(4.2309507,2.0017112,1.4266307,28.720975,1.0625,56.9375);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.15625,74.25);
			ctx.bezierCurveTo(1.202849,76.813384,1.237531,79.362751,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(23.88787,74.349853,23.952792,69.983782,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.9375,59.96875);
			ctx.bezierCurveTo(23.9353,59.645132,23.9403,59.323421,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.357838,26.874557,20.489705,2.0027508,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 13:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.3841817,2.02012,7.038344,5.7976314,5.3125,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.0907916,17.440015,3.9212735,18.396532,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.15625,23.0625);
			ctx.bezierCurveTo(1.8269954,32.688685,1.2208053,44.670407,1.0625,56.9375);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.15625,74.25);
			ctx.bezierCurveTo(1.202849,76.813384,1.237531,79.362751,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(23.88787,74.349853,23.952792,69.983782,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.9375,59.96875);
			ctx.bezierCurveTo(23.9353,59.645132,23.9403,59.323421,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.875,53.96875);
			ctx.bezierCurveTo(23.644417,41.888493,22.942112,30.280387,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.09375,10.15625);
			ctx.bezierCurveTo(17.434807,5.0589025,15.290079,2.0212978,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 14:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(6.9968252,2.0115915,3.9572188,13.864639,2.40625,30.09375);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.34375,46.21875);
			ctx.bezierCurveTo(1.2103843,49.742293000000004,1.1409945,53.344682,1.09375,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.15625,74.25);
			ctx.bezierCurveTo(1.202509,76.813747,1.237478,79.360989,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(23.887918,74.347912,23.9558,69.98375899999999,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.9375,59.96875);
			ctx.bezierCurveTo(23.9351,59.64634,23.9405,59.322221,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.84375,53.96875);
			ctx.bezierCurveTo(23.81153,52.31683,23.79978,50.661858,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.46875,41.9375);
			ctx.bezierCurveTo(23.313703,38.83089,23.114656,35.81123,22.875,32.90625);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.34375,27.09375);
			ctx.bezierCurveTo(20.708543,12.413682,17.668369,2.0128145,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		case 15:
			ctx.save();
			ctx.translate(x,y);
			if (!vertical) {
				ctx.translate(0,25);
				ctx.rotate(-Math.PI/2);
			}
			ctx.beginPath();
			ctx.moveTo(0,0);
			ctx.lineTo(25,0);
			ctx.lineTo(25,100);
			ctx.lineTo(0,100);
			ctx.closePath();
			ctx.clip();
			ctx.strokeStyle = 'rgba(0,0,0,0)';
			ctx.lineCap = 'butt';
			ctx.lineJoin = 'miter';
			ctx.miterLimit = 4;
			ctx.save();
			ctx.restore();
			ctx.save();
			ctx.translate(0,-952.3622);
			ctx.save();
			ctx.fillStyle = fill;
			ctx.strokeStyle = stroke;
			ctx.lineWidth = 1.1623140573501587;
			ctx.lineCap = "butt";
			ctx.lineJoin = "round";
			ctx.miterLimit = 4;
			ctx.translate(0,952.3622);
			ctx.beginPath();
			ctx.moveTo(12.5,2.03125);
			ctx.bezierCurveTo(9.389682,2.020139,7.0669469,5.8188299,5.34375,12.03125);
			ctx.lineTo(9.71875,14.6875);
			ctx.lineTo(4.28125,16.5);
			ctx.bezierCurveTo(4.091967,17.437376,3.9202414,18.399735,3.75,19.40625);
			ctx.lineTo(7.6875,22);
			ctx.lineTo(3.1875,23.0625);
			ctx.bezierCurveTo(2.8821987,25.276399,2.6086361,27.624548,2.375,30.0625);
			ctx.lineTo(7.5625,32.84375);
			ctx.lineTo(3.90625,35.125);
			ctx.lineTo(7.5625,38.15625);
			ctx.lineTo(4.28125,40.9375);
			ctx.lineTo(7.4375,44.21875);
			ctx.lineTo(1.375,46.21875);
			ctx.bezierCurveTo(1.2404747,49.742569,1.1763095,53.345060000000004,1.125,56.96875);
			ctx.lineTo(6.4375,60.625);
			ctx.lineTo(3.78125,66.9375);
			ctx.lineTo(5.9375,72.09375);
			ctx.lineTo(1.15625,74.25);
			ctx.bezierCurveTo(1.200457,76.813924,1.237452,79.360128,1.3125,81.84375);
			ctx.lineTo(4.15625,84.59375);
			ctx.lineTo(1.5,87.34375);
			ctx.bezierCurveTo(1.528813,88.083181,1.562871,88.836307,1.59375,89.5625);
			ctx.lineTo(4.9375,95.21875);
			ctx.lineTo(8.84375,90.90625);
			ctx.lineTo(11.5625,98);
			ctx.lineTo(17.59375,98);
			ctx.lineTo(23.25,93.3125);
			ctx.bezierCurveTo(23.28409,92.61447,23.31146,91.963254,23.34375,91.25);
			ctx.lineTo(19.6875,82.96875);
			ctx.lineTo(23.78125,78.59375);
			ctx.bezierCurveTo(23.887914,74.348089,23.955398,69.984492,23.96875,65.59375);
			ctx.lineTo(19.6875,63.65625);
			ctx.lineTo(23.9375,59.96875);
			ctx.bezierCurveTo(23.9351,59.646324,23.9405,59.322226,23.9375,59);
			ctx.lineTo(19.6875,56.71875);
			ctx.lineTo(23.84375,53.96875);
			ctx.bezierCurveTo(23.811318,52.317183,23.80029,50.660967,23.75,49.03125);
			ctx.lineTo(16.40625,46.09375);
			ctx.lineTo(23.4375,41.9375);
			ctx.bezierCurveTo(23.281542,38.837977,23.115062,35.805065,22.875,32.90625);
			ctx.lineTo(17.65625,28.9375);
			ctx.lineTo(22.34375,27.09375);
			ctx.bezierCurveTo(22.110095,25.001794,21.823432,22.994276,21.53125,21.09375);
			ctx.lineTo(16.53125,17.21875);
			ctx.lineTo(19.125,10.125);
			ctx.bezierCurveTo(17.465761,5.0484379,15.284125,2.0213191,12.5,2.03125);
			ctx.closePath();
			ctx.fill();
			ctx.stroke();
			ctx.restore();
			ctx.restore();
			ctx.translate(0,0);
			ctx.restore();
			break;
		}
		break;
	}
	if (damage == Math.pow(2, squares) - 1) {
		ctx.save();
		ctx.translate(x,y);
		if (!vertical) {
			ctx.translate(0,25);
			ctx.rotate(-Math.PI/2);
		}
		ctx.translate(0,0);
		ctx.fillStyle = "rgba(0,160,255,"+visibility+")";
  		ctx.fillRect(0,0,25,25*squares);
		ctx.restore();
	};
	ctx.restore();
};