/**
 * Includes core functionality of the server side - manages communication
 * between client and server.
 */
var express = require('express');
var database = require('./database');
var config = require('./config');
var util = require('util');
var connect = require('connect');
var parseCookie = connect.utils.parseCookie;
var MemoryStore = express.session.MemoryStore;
var sessionStore = new MemoryStore();
var Session = connect.middleware.session.Session;

var everyauth = require('everyauth');
everyauth.debug = false;

var gzip = require('connect-gzip');

everyauth.google
	.appId(config.auth.appId)
	.appSecret(config.auth.appSecret)
	.scope('https://www.googleapis.com/auth/userinfo.profile')
	.handleAuthCallbackError( function (req, res) {
		console.log('Handling auth callback error');
		// TODO - Think logic through and implement!
	})
	.findOrCreateUser(function (session, accessToken, accessTokenExtra, googleUserMetadata) {
		console.log('findOrCreateUser() handling');
		var promise = this.Promise();
		promise.fulfill(googleUserMetadata);
		
		var userid = googleUserMetadata.id;
		var username = googleUserMetadata.name;
		
		io.sockets.in(userid).emit('logout');		
		if (users[userid]) {
			users[userid].newSession = true;			
		} else {
			users[userid] = new user(userid, username);
		}
		
		addUserToDatabase(googleUserMetadata);
		return promise;
	})
	.handleLogout(function (req, res) {
		//remove user
		var username = req.session.auth.google.user.name;
		var userid = req.session.auth.google.user.id;
		console.log('Logout user '+username);
		if (users[userid]) {
			for(var gameToRemove in users[userid].games) {
				var game = games[gameToRemove];
				removeGame(gameToRemove);
				io.sockets.in(gameToRemove).emit('updategame', userid, {type: "quit", gameid: gameToRemove});
				io.sockets.emit('gameinfo', 'remove', game);
			}
			if (!users[userid].newSession) {
				delete users[userid];				
			} else {
				users[userid] = new user(userid, username);
			}
		}		
		updateUsers();
		updatePlayerStatus(userid);
		req.logout();
		this.redirect(res, this.logoutRedirectPath());
	})
	.redirectPath('/');

var app = express.createServer();
app.configure(function() {
	app.use(express.bodyParser());
	app.use(express.cookieParser());
	app.use(express.session({store: sessionStore, secret: 'secret', key: 'express.sid'}));
	
	var expirationTime = 86400000 * 7; // 7 days
	// TODO - I did not manage to add expires header for socket.io.js
	app.use(gzip.staticGzip(__dirname + '/public', {maxAge: expirationTime}));
	app.use(express.favicon(__dirname + '/public/favicon.ico', {maxAge: expirationTime})); 
	// In case we want to test without gzip	
	//	app.use(express.static(__dirname + '/public'));
	app.use(app.router);
	app.use(express.errorHandler());
	app.use(everyauth.middleware());
});

everyauth.helpExpress(app);

app.get("/cache.manifest", function(req, res){
	  res.header("Content-Type", "text/cache-manifest");
	  res.sendfile('cache.manifest'); 
});

app.get('/ping', function(req, res) {
    res.send('pong');
});

app.listen(config.port);

var io = require('socket.io').listen(app);

io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1);                    // reduce logging
io.set('transports', [                     // enable all transports (optional if you want flashsocket)
    'websocket'
  , 'flashsocket'
  , 'htmlfile'
  , 'xhr-polling'
  , 'jsonp-polling'
]);

io.set('authorization', function (data, accept) {
    if (data.headers.cookie) {
        data.cookie = parseCookie(data.headers.cookie);
        data.sessionID = data.cookie['express.sid'];
        // save the session store to the data object 
        // (as required by the Session constructor)
        data.sessionStore = sessionStore;
        sessionStore.get(data.sessionID, function (err, session) {
            if (err || !session) {
                accept('Error', false);
            } else {
                // create a session object, passing data as request and our
                // just acquired session data
                data.session = new Session(data, session);
                accept(null, true);
            }
        });
    } else {
       return accept('No cookie transmitted.', false);
    }
});

var games = {};
var users = {};

gameid_unique = 1;

io.sockets.on('connection', function (socket) {
	var hs = socket.handshake;
    console.log('A socket with sessionID ' + hs.sessionID + ' connected!');
	if (hs.session.auth) {
		//add user
		socket.emit('userauth', hs.session.auth);
		var username = hs.session.auth.google.user.name;
		var userid = hs.session.auth.google.user.id;
		socket.join(userid);
		socket.userid = userid;
		if (!users[userid]) users[userid] = new user(userid, username);
		updateUsers();
		updatePlayerStatus(userid);
	}
    // setup an inteval that will keep our session fresh
    var intervalID = setInterval(function () {
        // reload the session (just in case something changed,
        // we don't want to override anything, but the age)
        // reloading will also ensure we keep an up2date copy
        // of the session with our connection.
        hs.session.reload( function () { 
            // "touch" it (resetting maxAge and lastAccess)
            // and save it back again.
            hs.session.touch().save();
        });
    }, 60 * 1000);

	socket.on('senddata', function (data) {
		socket.broadcast.to(data.gameid).emit('updategame', socket.userid, data);
		if (data.type == 'end') {
			socket.emit('savegame', games[data.gameid]);
			socket.leave(data.gameid);
			delete users[socket.userid].games[data.gameid];
			saveGameResults(data.gameid, socket.userid, function(){refreshHistory(socket);});
		} else if (data.type == 'quit') {
			socket.leave(data.gameid);
			delete users[socket.userid].games[data.gameid];
		}
	});
	
	socket.on('getlobby', function() {
		socket.emit('updategames', games);
		socket.emit('updateusers', users);
	});
	
	socket.on('checkuser', function(userid) {
		var userLoggedIn = false;
		if (users[userid]) userLoggedIn = true;
		socket.emit('usercheck', userid, userLoggedIn);
	});
	
	socket.on('leavegame', function(gameid) {
		socket.emit('savegame', games[gameid]);
		endGame(socket, gameid);
	});

	socket.on('quitgame', function(gameid) {
		endGame(socket, gameid);
	});
	
	socket.on('newgame', function() {
		var gameid = newGame();
		var userid = socket.userid;
		console.log('User '+userid+' created game '+ gameid);
		joinGame(gameid, userid);
		socket.join(gameid);
		users[userid].games[gameid] = gameid;
		socket.emit('gamecreated', gameid);
		socket.emit('updateuser', users[userid]);		
		//updateGames();
		io.sockets.emit('gameinfo', 'add', games[gameid]);
	});
	
	socket.on('getgameinfo', function(gameid) {
		socket.emit('gamedata', games[gameid]);
	});
	
	socket.on('joingame', function(gameid) {
		var userid = socket.userid;
		console.log('User '+userid+' tries to join game '+ gameid);
		var startGame = joinGame(gameid, userid);
		//updateGames();
		io.sockets.emit('gameinfo', 'change', games[gameid]);
		if (startGame) {
			console.log('User '+userid+' joins and starts game '+ gameid);
			socket.emit('gamecreated', gameid);
			socket.emit('updateuser', users[userid]);
			socket.join(gameid);
			users[userid].games[gameid] = gameid;
			var startingUserId = Math.floor(Math.random()*2) ? games[gameid].player1.id : games[gameid].player2.id;
			io.sockets.in(gameid).emit('startgame', {"startingUserId":startingUserId, "gameid": gameid});
		};
	});
	
	socket.on('removegame', function(gameid) {
		removeGame(gameid);
	});

	socket.on('gethistoryentries', function() {
		updateHistoryEntries(socket, false);
	});

	socket.on('getchart', function() {
		updateChart(socket, false);
	});
	
	socket.on('getuserrank', function() {
		getUserRank(socket);
	});

	socket.on('savehistoryentry', function(historyEntry) {
		database.saveHistoryEntry(historyEntry, function(historyEntries){
			socket.emit('historyentrysaved');
		});
	});

	socket.on('disconnect', function(){
		console.log('A socket with sessionID ' + hs.sessionID + ' disconnected!');
	    // clear the socket interval to stop refreshing the session
	    clearInterval(intervalID);
	    var userid = socket.userid;    
		if (users[userid]) {
			for(var gameToRemove in users[userid].games) {
				console.log('gameToRemove: '+gameToRemove);
				var game = games[gameToRemove];
				removeGame(gameToRemove);
				socket.leave(gameToRemove);
				socket.broadcast.to(gameToRemove).emit('updategame', userid, {type: "quit", gameid: gameToRemove});
				io.sockets.emit('gameinfo', 'remove', game);
			}			
			if (!users[userid].newSession) delete users[userid];
			updateUsers();
			updatePlayerStatus(userid);
		};
	});
});

function addUserToDatabase(userdata) {
	var userEntry = {id: userdata.id, username: userdata.name};
	database.addOrUpdateUser(userEntry, function(){});
}

function endGame(socket, gameid) {
	if (users[socket.userid]) {
		socket.leave(gameid);
		delete users[socket.userid].games[gameid];
		if (games[gameid]) io.sockets.emit('gameinfo', 'remove', games[gameid]);
		removeGame(gameid);
	}
}

function updateHistoryEntries(socket, toAll) {
		database.getHistoryEntries(function(historyEntries){
			if (toAll) io.sockets.emit('updatehistory', historyEntries);
			else socket.emit('updatehistory', historyEntries);
		});
}

function updateChart(socket, toAll) {
	database.getChartEntries(function(chartEntries){
		for(var i in chartEntries){
			var chartEntry = chartEntries[i];
			var onlineStatus = isPlayerOnline(chartEntry.uid) ? 'online' : 'offline';
			chartEntry['online'] = onlineStatus;
		}
		if (toAll) io.sockets.emit('updatechart', chartEntries);
		else socket.emit('updatechart', chartEntries);
	});
}

function getUserRank(socket) {
	database.getUserRank(socket.userid, function(userRank){
		var rank = userRank[0] ? userRank[0].rank : 0; 
		socket.emit('updateuserrank', rank);
	});
}

function refreshHistory(socket) {
	updateHistoryEntries(socket, true);
	updateChart(socket, true);
}

function user(id, username) {
	this.id = id;
	this.username = username;
	this.games = {};
	this.newSession = false;
}

function game(id) {
	this.id = id;
	this.date = new Date();
	this.player1 = null;
	this.player2 = null;
}

function newGame() {
	var gameid = gameid_unique++;
	games[gameid] = new game(gameid);
	return gameid;
}

function joinGame(gameid, userid) {
	var gameToJoin = games[gameid];
	var username = users[userid].username;
	if (!gameToJoin.player1) {
		gameToJoin.player1 = {id: userid, name: username};
		games[gameid] = gameToJoin;
		if (gameToJoin.player2) return true;
		else return false;
	} else if (!gameToJoin.player2) {		
		gameToJoin.player2 = {id: userid, name: username};
		games[gameid] = gameToJoin;
		return true;
	} else {
		return false;
	};
}

function removeGame(gameid) {
	if (games[gameid]) delete games[gameid];
}

function saveGameResults(gameid, winnerid, callback) {
	var historyEntry = {};
	var gameToSave = games[gameid];
	historyEntry.begin_time = gameToSave.date;
	historyEntry.winner = winnerid;
	if (gameToSave.player1.id == winnerid) {
		historyEntry.loser = gameToSave.player2.id;
	} else {
		historyEntry.loser = gameToSave.player1.id;
	}
	database.saveHistoryEntry(historyEntry, callback);
}

function updateGames() {
	io.sockets.emit('updategames', games);
}

function updateUsers() {
	io.sockets.emit('updateusers', users);
}

/**
 * Checks if player with particular user name is online.
 * 
 * @param username
 * @returns {Boolean}
 */
function isPlayerOnline(userid) {
    if (users[userid]) return true;
    return false;
};

function updatePlayerStatus(userid) {
	var status = 'offline';
	if (users[userid]) status = 'online';
	io.sockets.emit('updateplayerstatus', userid, status);
};