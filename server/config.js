/**
 * Includes all the necessary configuration for the application.
 */
var config = {};

// Location of the application:
config.host = 'localhost';
config.port = 8082;

// Database-specific configuration
config.db = {};
config.db.vendor = 'postgres';
config.db.name = 'vrl2012';
config.db.username = 'postgres';
config.db.password = 'postgres';
config.db.host = 'localhost';
config.db.port = '5432';

// Authentication specific configuration
config.auth = {};
config.auth.appId = '439485769063.apps.googleusercontent.com';
config.auth.appSecret = 'gA2ST1EZ_Kw4MLSvPxiQJ000';

module.exports = config;