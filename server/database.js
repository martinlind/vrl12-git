/**
 * Encapsulates logic associated with database.
 */
var database = {};
var pg = require('pg');
var config = require ('./config');

var conString = config.db.vendor + '://' +
config.db.username + ':' +
config.db.password+ '@' + 
config.db.host + ':' + 
config.db.port+ '/' + 
config.db.name;

var client = new pg.Client(conString);
client.connect();

/**
 * Gets history entries from the database in the descending chronological order
 * 
 * @param callback - lets the caller know that operation is performed,
 * invocation also includes data got from the database.
 */
database.getHistoryEntries = function(callback){
	var client = getAndConnectClient();
	
	// XXX I could not put it into SQL function, that is why it is copied here in quite robust manner.
	var query = client.query("SELECT " +
		"history.begin_time, " +
		"history.winner, " +
		"history.loser, " +
		"winner_name.name AS winner_name, " + 
		"loser_name.name AS loser_name " +
	"FROM history " +
	"LEFT JOIN chart AS winner_name " +
		"ON history.winner=winner_name.uid " +
	"LEFT JOIN chart AS loser_name " +
		"ON history.loser=loser_name.uid " +
	"ORDER BY begin_time DESC LIMIT 10;");
	handleSelect(function(rawResult){
		// Here the result of the select query needs some restructuring
		var historyEntries = [];
		for(var i in rawResult){
			var rawEntry = rawResult[i];
			var historyEntry = {
					'begin_time':rawEntry.begin_time,
					'winner':{'id':rawEntry.winner, 'name':rawEntry.winner_name},
					'loser':{'id':rawEntry.loser, 'name':rawEntry.loser_name}
			};
			historyEntries.push(historyEntry);
		}
		callback(historyEntries);
	}, client, query);
};

database.getChartEntries = function(callback){
	var client = getAndConnectClient();
	var query = client.query("SELECT * FROM chart ORDER BY rank DESC LIMIT 10");
	handleSelect(callback, client, query);
};

database.getUserRank = function(userid, callback){
	var client = getAndConnectClient();
	var query = client.query("SELECT rank FROM chart WHERE uid = $1", [userid]);
	handleSelect(callback, client, query);
};

/**
 * Saves history entry to the database. SQL function 'update_history()' does
 * the work.
 * 
 * @param historyEntry - history entry to be saved.
 * @param callback - lets the caller know that operation is performed.
 */
database.saveHistoryEntry = function(historyEntry, callback){
	var client = getAndConnectClient();
	
	var query = client.query("SELECT update_history($1, $2, $3)", 
			[historyEntry.begin_time, historyEntry.winner, historyEntry.loser]);
	
	query.on('end', function() { 
		client.end();
		callback();
	});
};

/**
 * Adds user entry to the database or updates user name in case user has 
 * changed his/her Google user name.
 * 
 * @param userEntry
 * @param callback
 */
database.addOrUpdateUser = function(userEntry, callback){
	var client = getAndConnectClient();
	
	var query = client.query("SELECT add_or_update_user($1, $2)", 
			[userEntry.id, userEntry.username]);
	
	query.on('end', function() { 
		client.end();
		callback();
	});
};

/**
 * Composes and returns the client for the database.
 * 
 * XXX It may be pretty expensive to create new client for each query, but it
 * is just a workaround to solve the problem related to multiple queries so 
 * far.
 * 
 * @returns {pg.Client}
 */
function getAndConnectClient(){
	var client = new pg.Client(conString);
	client.connect();
	return client;
}

/**
 * Processes results of the SELECT query and returns it via callback.
 * 
 * @param callback
 * @param client
 * @param query
 */
function handleSelect(callback, client, query){
	var result = [];
	query.on('row', function(row) {
		result.push(row);
	});
	
	query.on('end', function() { 
		client.end();
		callback(result);
	});	
}

module.exports = database;