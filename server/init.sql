-- Creates the database with necessary tables and functions.

-- Creating necessary tables - start
CREATE TABLE history(
	id    		SERIAL PRIMARY KEY,
	begin_time	timestamp NOT NULL,
	winner		varchar(25) NOT NULL,
	loser		varchar(25) NOT NULL
);

CREATE TABLE chart(
	uid 		varchar(25) NOT NULL PRIMARY KEY,
	name		varchar(128),
	won			integer NOT NULL,
	lost		integer NOT NULL,
	rank    	integer NOT NULL
);
-- Creating necessary tables - end

CREATE OR REPLACE FUNCTION update_history(timestamp with time zone, varchar(25), varchar(25))
RETURNS void AS $$
DECLARE
	btime ALIAS FOR $1;
    wnr ALIAS FOR $2;
    lsr ALIAS FOR $3;
BEGIN
    INSERT INTO history (begin_time, winner, loser)  VALUES
    (btime, wnr, lsr);
 
    PERFORM update_chart(wnr, true);  
    PERFORM update_chart(lsr, false);  
END;
$$  LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_chart(varchar(25), boolean)
RETURNS void AS $$
DECLARE
	player ALIAS FOR $1;
	is_winner ALIAS FOR $2;
	
	games_won int;
	games_lost int;
	games_total int;
	won_and_total_relation decimal;

	rank_multiplier int;
	player_rank int;
BEGIN
	SELECT INTO games_won, games_lost 
	won, lost
	FROM chart WHERE uid = player;
	
	IF NOT FOUND THEN
      	IF is_winner THEN
      		games_won = 1;
      		games_lost = 0;
      	ELSE
      		games_won = 0;
      		games_lost = 1;
      	END IF;
  	ELSE
	  	IF is_winner THEN
      		games_won = games_won + 1;
      	ELSE
      		games_lost = games_lost + 1;
      	END IF;  			
    END IF;
	
    games_total = games_won + games_lost;
    won_and_total_relation = games_won::float / games_total;
    
    -- Calculate rank (Algorithm - win/total relation multiplied by 1000 + number of games)
    rank_multiplier = 1000;
    player_rank = CAST((won_and_total_relation * rank_multiplier + games_total) AS integer);
    
	UPDATE chart
	SET won=games_won, lost=games_lost, rank=player_rank
	WHERE uid=player;
END;
$$  LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_or_update_user(varchar(25), text)
RETURNS void AS $$
DECLARE
	user_id ALIAS FOR $1;
    username ALIAS FOR $2;
BEGIN
	PERFORM *
	FROM chart WHERE uid = user_id;
	
	IF NOT FOUND THEN
      	INSERT INTO chart VALUES (user_id, username, 0,0,0);
  	ELSE
     	UPDATE chart
    	SET name=username
    	WHERE uid=user_id; 			
    END IF;
END;
$$  LANGUAGE plpgsql;