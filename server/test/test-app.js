var should = require('should');
var io = require('socket.io-client');

// Assumes that tests are located under sub-directory 'test' of the application.
var config = require ('../config');

var socketURL = 'http://' + config.host + ':' + config.port;

var options = {
	transports : [ 'websocket' ],
	'force new connection' : true
};

var username1 = "Peeter";
var username2 = "Jaan";

var gameName = "theGame";

var dataSecondToFirst = {
	type : 'data',
	content : "secondToFirst"
};

describe("Server core", function() {
	/**
	 * Automated procedure to simulate a move from one player to the other.
	 */
	it('Should add users, add game and make move from one player to other',
			function(done) {

				var player1 = null, player2 = null;

				// Verifying the exit criterion for the test and disconnecting
				var completeTest = function(data) {
					data.type.should.equal('data');
					data.content.should.equal('secondToFirst');
					player1.disconnect();
					player2.disconnect();
					done();
				};

				player1 = io.connect(socketURL, options);

				player1.on('connect', function(data) {
					player1.emit('adduser', username1);

					player2 = io.connect(socketURL, options);
					player2.on('connect', function(data) {
						player2.emit('adduser', username2);

						player1.emit('newgame', gameName);
						player2.emit('newgame', gameName);

						player2.emit('senddata', dataSecondToFirst);

						player1.on('updategame', function(username, data) {
							completeTest(data);
				});
			});
		});
	});
});
describe("Database", function() {
	/**
	 * Test case for reading history entries from the database
	 */
	it('Should get history entries from the database', function(done) {
		var client = io.connect(socketURL, options);
		client.on('connect', function(data) {
			client.emit('gethistoryentries');

			client.on('updatehistory',
				function(historyEntries) {
					var i = 0;
					for (i in historyEntries) {
						console.log("History entry data:");
						var historyEntry = historyEntries[i];

						// At first, just verify data visually
						console.log("\tHistory entry begin time: "
								+ historyEntry.begin_time);
						console.log("\tHistory entry winner: "
								+ historyEntry.winner);
						console.log("\tHistory entry loser: "
								+ historyEntry.loser);
					}
					client.disconnect();
					done();
			});
		});
	});
	/**
	 * Invokes the process where the history entry should be saved into the
	 * database. Currently final verification should be done visually on
	 * the database.
	 */
	it('Should save history entry into the database', function(done) {
		var beginTime = new Date();
		var winner = 'Arvydas';
		var loser = 'Vytautas';
			
		var historyEntry = {};
		historyEntry.begin_time = beginTime;
		historyEntry.winner = winner;
		historyEntry.loser = loser;
			
		var client = io.connect(socketURL, options);
		client.on('connect', function(data) {
			client.emit('savehistoryentry', historyEntry);
			
			client.on('historyentrysaved', function(){
				client.disconnect();
				done();
			});
		});	
	});
	/**
	 * Test case for reading chart from the database
	 */
	it('Should get chart from the database', function(done) {
		var client = io.connect(socketURL, options);
		client.on('connect', function(data) {
			client.emit('getchart');

			client.on('updatechart',
				function(chartEntries) {
					var i = 0;
					for (i in chartEntries) {
						console.log("Chart entry data:");
						var chartEntry = chartEntries[i];

						// At first, just verify data visually
						console.log("\tChart entry - player: "
								+ chartEntry.player);
						console.log("\tChart entry - won: "
								+ chartEntry.won);
						console.log("\tChart entry - lost: "
								+ chartEntry.lost);
						console.log("\tChart entry - rank: "
								+ chartEntry.rank);
					}
					client.disconnect();
					done();
			});
		});
	});
	/**
	 * Test case for getting user rank from database
	 */
	it('Should get user rank from the database', function(done) {
		var client = io.connect(socketURL, options);
		client.on('connect', function(data) {
			client.emit('adduser', 'Arvydas');
			client.emit('getuserrank');

			client.on('updateuserrank',
				function(userRank) {
					console.log("User rank: "+ userRank);
					client.disconnect();
					done();
			});
		});
	});
});